const path = require('path')
const NextI18Next = require('next-i18next').default
const { localeSubpaths } = require('next/config').default().publicRuntimeConfig

// console.log(path.resolve('./public/static/locales'))

module.exports = new NextI18Next({
  defaultLanguage: 'ru',
  otherLanguages: ['en', 'he', 'zh'],
  localeSubpaths: localeSubpaths,
  localePath: path.resolve('./public/static/locales'),
  backend: {
    loadPath: '/static/locales/{{lng}}/{{ns}}.json',
  },
  detection: {
    lookupCookie: 'next-i18next',
    order: ['cookie', 'querystring', 'localStorage', 'path', 'subdomain'],
    caches: ['cookie'],
  },
  shallowRender: true
})