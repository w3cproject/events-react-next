import React from 'react';
import { Link } from 'i18n';
import { Divider, Image } from 'antd';
import Logo from 'components/UI/Logo/Logo';
import { REGISTRATION_PAGE } from 'settings/constant';
import SignInForm from './SignInForm';
import LazyLoad from 'react-lazyload'
import SocialLogin from '../SocialLogin';
import Wrapper, {
  Title,
  TitleInfo,
  Text,
  FormWrapper,
  BannerWrapper,
} from '../Auth.style';
// demo image
import signInImage from 'assets/images/login-page-bg.jpg';
import eventsLogoImg from 'assets/images/events_logo_black.svg';

const SignIn = ({ t }) => {
  return (
    <Wrapper>
      <FormWrapper>
        <Logo withLink linkTo="/" src={eventsLogoImg} title="Events SignIn" />
        <Title>{t('welcomeBack')}</Title>
        <TitleInfo>{t('pleaseLogin')}</TitleInfo>
        <SignInForm t={t} />
        {/* <Divider>Or log in with </Divider>
        <SocialLogin /> */}
        <Text style={{ marginTop: '30px' }}>
          {t('noAccount')}&nbsp;
          <Link href={REGISTRATION_PAGE}>
            <a>{t('registration')}</a>
          </Link>
        </Text>
      </FormWrapper>
      <BannerWrapper>
      <LazyLoad height="100vh" once overflow={true}>
          <Image
            src={signInImage}
            preview={false}
            placeholder={true}
          />
        </LazyLoad>
      </BannerWrapper>
    </Wrapper>
  );
};

export default SignIn
