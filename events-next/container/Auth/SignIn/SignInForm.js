import React, { useContext, useState } from 'react';
import { Link } from 'i18n';
import { useForm, Controller } from 'react-hook-form';
import { MdLockOpen } from 'react-icons/md';
import { Input, Switch, Button } from 'antd';
import FormControl from 'components/UI/FormControl/FormControl';
import { AuthContext } from 'context/AuthProvider';
import { FORGET_PASSWORD_PAGE } from 'settings/constant';
import { FieldWrapper, SwitchWrapper, Label, ErrorMessage } from '../Auth.style';
import Loader from 'components/Loader/Loader'
import { Spin } from 'antd'

const SignInForm = ({ t }) => {
  const { signIn } = useContext(AuthContext);
  const { control, errors, setError, handleSubmit } = useForm();
  const [errorMessage, setErrorMessage] = useState('')
  const [loader, setLoader] = useState(false)

  const onSubmit = async (data) => {
    setLoader(true)
    const res = await fetch(`${process.env.API_URL}/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    const resData = await res.json()
    console.log(resData)

    if (resData.status === 'wrong' || resData.status === 'not_exists') {
      setErrorMessage(t('wrongCredentials'))
      setLoader(false)
      return false
    }
    setErrorMessage('')

    signIn(resData)
  };

  return (
    <Spin spinning={loader}>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* <Loader /> */}
        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
        <FormControl
          label="Email"
          htmlFor="email"
          error={
            errors.email && (
              <>
                {errors.email?.type === 'required' && (
                  <span>{t('requiredField')}</span>
                )}
                {errors.email?.type === 'pattern' && (
                  <span>{t('validEmailField')}</span>
                )}
              </>
            )
          }
        >
          <Controller
            as={<Input />}
            type="email"
            id="email"
            name="email"
            defaultValue=""
            control={control}
            rules={{
              required: true,
              pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
            }}
          />
        </FormControl>
        <FormControl
          label={t('password')}
          htmlFor="password"
          error={
            errors.password && (
              <>
                {errors.password?.type === 'required' && (
                  <span>{t('requiredField')}</span>
                )}
                {errors.password?.type === 'minLength' && (
                  <span>{t('passwordMinLength')}</span>
                )}
              </>
            )
          }
        >
          <Controller
            as={<Input.Password />}
            defaultValue=""
            control={control}
            id="password"
            name="password"
            rules={{ required: true, minLength: 6 }}
          />
        </FormControl>
        <FieldWrapper>
          <SwitchWrapper>
            <Controller
              as={<Switch />}
              name="rememberMe"
              defaultValue={false}
              valueName="checked"
              control={control}
            />
            <Label>{t('rememberMe')}</Label>
          </SwitchWrapper>
          <Link href={FORGET_PASSWORD_PAGE}>
            <a>{t('forgetPassword')}</a>
          </Link>
        </FieldWrapper>
        <Button
          className="signin-btn"
          type="primary"
          htmlType="submit"
          size="large"
          style={{ width: '100%' }}
        >
          <MdLockOpen />
          {t('login')}
        </Button>
      </form>
    </Spin>
  );
};

export default SignInForm;
