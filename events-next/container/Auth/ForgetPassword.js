import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { MdEmail } from 'react-icons/md';
import { Input, Button, Spin } from 'antd';
import Logo from 'components/UI/Logo/Logo';
import FormControl from 'components/UI/FormControl/FormControl';
import Wrapper, {
  Title,
  TitleInfo,
  FormWrapper,
  BannerWrapper,
  ErrorMessage,
  SuccessMessage
} from './Auth.style';
// demo images
import authBgImg from 'assets/images/login-page-bg.jpg';
import eventsLogoImg from 'assets/images/events_logo_black.svg';

export default function ForgetPassWord() {
  const { control, errors, reset, setValue, handleSubmit } = useForm({
    mode: 'onChange',
  });
  const [loader, setLoader] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [successMessage, setSuccessMessage] = useState('')

  const onSubmit = async (data) => {
    setLoader(true)
    setErrorMessage('')
    setSuccessMessage('')

    const req = await fetch(`${process.env.API_URL}/auth/forget`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })

    const res = await req.json()

    setTimeout(() => {
      if (res.status === 'success') {
        reset({ email: '' })
        setSuccessMessage('Письмо с восстановлением пароля отправлено на указанную почту')
      }

      if (res.status === 'fail') {
        setErrorMessage('Такая почта не зарегистрирована')
      }

      setLoader(false)
    }, 2000)
  };

  return (
    <Wrapper>
      <FormWrapper>
        <Logo withLink linkTo="/" src={eventsLogoImg} title="Events forget password" />
        <Title>С возвращением</Title>
        <TitleInfo>Введите ваш email для восстановления</TitleInfo>
        <Spin spinning={loader}>
          <form onSubmit={handleSubmit(onSubmit)}>
          {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
          {successMessage && <SuccessMessage>{successMessage}</SuccessMessage>}
            <FormControl
              label="Email"
              htmlFor="email"
              error={
                errors.email && (
                  <>
                    {errors.email?.type === 'required' && (
                      <span>Поля обязательно для заполнения!</span>
                    )}
                    {errors.email?.type === 'pattern' && (
                      <span>Введите корректный адрес эл. почты!</span>
                    )}
                  </>
                )
              }
            >
              <Controller
                as={<Input />}
                type="email"
                id="email"
                name="email"
                defaultValue=""
                control={control}
                rules={{
                  required: true,
                  pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                }}
              />
            </FormControl>
            <Button
              className="signin-btn"
              type="primary"
              htmlType="submit"
              size="large"
              style={{ width: '100%' }}
            >
              <MdEmail />
            Отправить
          </Button>
          </form>
        </Spin>
      </FormWrapper>
      <BannerWrapper>
        <div
          style={{
            backgroundImage: `url(${authBgImg})`,
            backgroundPosition: 'center center',
            height: '100vh',
            backgroundSize: 'cover',
          }}
        />
      </BannerWrapper>
    </Wrapper>
  );
}
