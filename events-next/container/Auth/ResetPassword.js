import React, { useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { MdEmail } from 'react-icons/md';
import { Input, Button, Spin } from 'antd';
import Logo from 'components/UI/Logo/Logo';
import FormControl from 'components/UI/FormControl/FormControl';
import Wrapper, {
  Title,
  TitleInfo,
  FormWrapper,
  BannerWrapper,
  ErrorMessage,
  SuccessMessage
} from './Auth.style';
// demo images
import authBgImg from 'assets/images/login-page-bg.jpg';
import eventsLogoImg from 'assets/images/events_logo_black.svg';

export default function ForgetPassWord({ token }) {
  const { control, errors, reset, setValue, watch, handleSubmit } = useForm({
    mode: 'onChange',
  });
  const [loader, setLoader] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [successMessage, setSuccessMessage] = useState('')

  const password = watch('password');
  const confirmPassword = watch('confirmPassword');

  const onSubmit = async (data) => {
    setLoader(true)
    setErrorMessage('')
    setSuccessMessage('')

    const req = await fetch(`${process.env.API_URL}/auth/reset`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify({
        password: data.password
      })
    })

    const res = await req.json()

    setTimeout(() => {
      if (res.status === 'success') {
        reset({ password: '', confirmPassword: '' })
        setSuccessMessage('Пароль успешно изменен')
      }

      if (res.msg === 'Signature verification failed') {
        setErrorMessage('Ссылка для восстановления не доступна, попробуйте запросить восстановление еще раз.')
      }

      setLoader(false)
    }, 2000)
  };

  return (
    <Wrapper>
      <FormWrapper>
        <Logo withLink linkTo="/" src={eventsLogoImg} title="Events reset password" />
        <Title>С возвращением</Title>
        <TitleInfo>Придумайте новый пароль</TitleInfo>
        <Spin spinning={loader}>
          <form onSubmit={handleSubmit(onSubmit)}>
            {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
            {successMessage && <SuccessMessage>{successMessage}</SuccessMessage>}
            <FormControl
              label="Пароль"
              htmlFor="password"
              error={
                errors.password && (
                  <>
                    {errors.password?.type === 'required' && (
                      <span>Поля обязательно для заполнения!</span>
                    )}
                    {errors.password?.type === 'minLength' && (
                      <span>Пароль должен быть больше 6 символов!</span>
                    )}
                  </>
                )
              }
            >
              <Controller
                as={<Input.Password />}
                defaultValue=""
                control={control}
                id="password"
                name="password"
                rules={{ required: true, minLength: 6 }}
              />
            </FormControl>
            <FormControl
              label="Confirm password"
              htmlFor="confirmPassword"
              error={
                password &&
                password !== confirmPassword && (
                  <span>Пароли не совпадают!</span>
                )
              }
            >
              <Controller
                as={<Input.Password />}
                defaultValue=""
                control={control}
                id="confirmPassword"
                name="confirmPassword"
                rules={{ required: true, minLength: 6 }}
              />
            </FormControl>
            <Button
              className="signin-btn"
              type="primary"
              htmlType="submit"
              size="large"
              style={{ width: '100%' }}
            >
              Сохранить
          </Button>
          </form>
        </Spin>
      </FormWrapper>
      <BannerWrapper>
        <div
          style={{
            backgroundImage: `url(${authBgImg})`,
            backgroundPosition: 'center center',
            height: '100vh',
            backgroundSize: 'cover',
          }}
        />
      </BannerWrapper>
    </Wrapper>
  );
}
