import React from 'react';
import { Link } from 'i18n';
import { Divider, Image } from 'antd';
import Logo from 'components/UI/Logo/Logo';
import { LOGIN_PAGE } from 'settings/constant';
import SignUpForm from './SignUpForm';
import LazyLoad from 'react-lazyload'
import SocialLogin from '../SocialLogin';
import Wrapper, {
  Title,
  TitleInfo,
  Text,
  FormWrapper,
  BannerWrapper,
} from '../Auth.style';
// demo image
import signUpImage from 'assets/images/login-page-bg.jpg';
import eventsLogoImg from 'assets/images/events_logo_black.svg';

const SignUp = ({ t }) => {
  return (
    <Wrapper>
      <FormWrapper>
        <Logo withLink linkTo="/" src={eventsLogoImg} title="Events SignUp" />
        <Title>{t('welcomeBack')}</Title>
        <TitleInfo>{t('pleaseRegister')}</TitleInfo>
        <SignUpForm t={t} />
        {/* <Divider>Or Register Up With </Divider>
        <SocialLogin /> */}
        <Text style={{ marginTop: '30px' }}>
          {t('haveAccount')} &nbsp;
          <Link href={LOGIN_PAGE}>
            <a>{t('login')}</a>
          </Link>
        </Text>
      </FormWrapper>
      <BannerWrapper>
        <LazyLoad height="100vh" once overflow={true}>
          <Image
            src={signUpImage}
            preview={false}
            placeholder={true}
          />
        </LazyLoad>
      </BannerWrapper>
    </Wrapper>
  );
};

export default SignUp
