import React, { useContext, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { MdLockOpen } from 'react-icons/md';
import { Input, Switch, Button, Typography } from 'antd';
import FormControl from 'components/UI/FormControl/FormControl';
import { AuthContext } from 'context/AuthProvider';
import { FieldWrapper, SwitchWrapper, Label, ErrorMessage } from '../Auth.style';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';
import { Spin } from 'antd'
import { Link } from 'i18n'

const { Text } = Typography

const SignUpForm = ({ t }) => {
  const { signUp } = useContext(AuthContext);
  const { control, watch, errors, handleSubmit } = useForm({
    mode: 'onChange',
  });
  const [errorMessage, setErrorMessage] = useState('')
  const [loader, setLoader] = useState(false)
  const password = watch('password');
  const confirmPassword = watch('confirmPassword');

  const onSubmit = async (data) => {
    setLoader(true)
    const res = await fetch(`${process.env.API_URL}/auth/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    const resData = await res.json()

    if (resData.status === 'fail') {
      setErrorMessage(t('emailExists'))
      setLoader(false)
      return false
    }

    setErrorMessage('')
    signUp(resData);
  };
  return (
    <Spin spinning={loader}>
      <form onSubmit={handleSubmit(onSubmit)}>
        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
        <FormControl
          label="Email"
          htmlFor="email"
          error={
            errors.email && (
              <>
                {errors.email?.type === 'required' && (
                  <span>{t('requiredField')}</span>
                )}
                {errors.email?.type === 'pattern' && (
                  <span>{t('validEmailField')}</span>
                )}
              </>
            )
          }
        >
          <Controller
            as={<Input />}
            type="email"
            id="email"
            name="email"
            defaultValue=""
            control={control}
            rules={{
              required: true,
              pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
            }}
          />
        </FormControl>
        <FormControl
          label={t('password')}
          htmlFor="password"
          error={
            errors.password && (
              <>
                {errors.password?.type === 'required' && (
                  <span>{t('requiredField')}</span>
                )}
                {errors.password?.type === 'minLength' && (
                  <span>{t('passwordMinLength')}</span>
                )}
              </>
            )
          }
        >
          <Controller
            as={<Input.Password />}
            defaultValue=""
            control={control}
            id="password"
            name="password"
            rules={{ required: true, minLength: 6, maxLength: 20 }}
          />
        </FormControl>
        <FormControl
          label={t('confirmPassword')}
          htmlFor="confirmPassword"
          error={
            password &&
            password !== confirmPassword && (
              <span>{t('samePassword')}</span>
            )
          }
        >
          <Controller
            as={<Input.Password />}
            defaultValue=""
            control={control}
            id="confirmPassword"
            name="confirmPassword"
            rules={{ required: true, minLength: 6 }}
          />
        </FormControl>
        <FieldWrapper>
          <SwitchWrapper>
            <Controller
              as={<Switch />}
              name="rememberMe"
              defaultValue={false}
              valueName="checked"
              control={control}
            />
            <Label>{t('rememberMe')}</Label>
          </SwitchWrapper>
        </FieldWrapper>
        <FieldWrapper>
        <SwitchWrapper>
            <Controller
              as={<Switch checkedChildren={<CheckOutlined />}
                unCheckedChildren={<CloseOutlined />} />}
              name="termsAndConditions"
              defaultValue={false}
              valueName="checked"
              control={control}
              rules={{ required: true }}
            />
            {/* <Label>{t('termsAgreement')}</Label> */}
            <Label>
              <span>Даю согласие на </span>
              <a href="https://events.com/privacy.pdf" target="_blank">
                Обработку персональных данных
            </a>
              <span> и принимаю условия </span>
              <a href="https://events.com/eula.pdf" target="_blank">
                Пользовательского соглашения
            </a>
            </Label>
          </SwitchWrapper>
        </FieldWrapper>
        <div style={{ marginBottom: '20px', textAlign: 'center' }}>
          {errors.termsAndConditions && <Text type="danger">{t('termsErrorMessage')}</Text>}
        </div>
        <Button
          className="signin-btn"
          type="primary"
          htmlType="submit"
          size="large"
          style={{ width: '100%' }}
        >
          <MdLockOpen />
          {t('registration')}
        </Button>
      </form>
    </Spin>
  );
};


export default SignUpForm