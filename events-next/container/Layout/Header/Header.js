import React, { useState, useContext } from 'react';
import { Link } from 'i18n';
import { withRouter } from 'next/router';
import dynamic from 'next/dynamic';
import PropTypes from 'prop-types'
import Sticky from 'react-stickynode';
import { IoIosClose } from 'react-icons/io';
import Logo from 'components/UI/Logo/Logo';
import Text from 'components/UI/Text/Text';
import { Button, Drawer, Avatar } from 'antd';
import Navbar from 'components/Navbar/Navbar';
import { LayoutContext } from 'context/LayoutProvider';
import { AuthContext } from 'context/AuthProvider';
import { USER_PROFILE_PAGE } from 'settings/constant';
import HeaderWrapper, {
  MobileNavbar,
  CloseDrawer,
  AvatarWrapper,
  AvatarImage,
  AvatarInfo,
  LogoArea,
} from './Header.style';
import { withTranslation } from 'i18n'
// dummy images
import eventsLogoImgBlack from 'assets/images/events_logo_black.svg';
import eventsLogoImgWhite from 'assets/images/events_logo_white.svg';

const AuthMenu = dynamic(() => import('./AuthMenu'));
const MainMenu = dynamic(() => import('./MainMenu'));
const MobileMenu = dynamic(() => import('./MobileMenu'));
const ProfileMenu = dynamic(() => import('./ProfileMenu'));
const NavbarSearch = dynamic(() => import('./NavbarSearch'));

const Header = ({ t, router }) => {
  // const { t } = useTranslation('common')
  const { loggedIn, user } = useContext(AuthContext);
  const [{ searchVisibility }] = useContext(LayoutContext);
  const [state, setState] = useState(false);
  const sidebarHandler = () => {
    setState((state) => !state);
  };

  const headerType = router.pathname === '/' ? 'transparent' : 'default';
  const AvatarImg = (loggedIn && user.avatar) ? process.env.AMAZON_ASSETS_URL + '/' + user.avatar : '';

  return (
    <HeaderWrapper>
      <Sticky top={0} innerZ={1001} activeClass="isHeaderSticky">
        <Navbar
          logo={
            <>
              {/* {headerType === 'transparent' && <img src={eventsLogoImgWhite} />} */}
              <Logo withLink linkTo="/" colorTypeClass="logo-white" src={eventsLogoImgWhite} title="Events" />
              <Logo withLink linkTo="/" colorTypeClass="logo-black" src={eventsLogoImgBlack} title="Events" />
            </>
          }
          navMenu={<MainMenu />}
          authMenu={<AuthMenu />}
          isLogin={loggedIn}
          avatar={<Logo src={AvatarImg} />}
          profileMenu={<ProfileMenu avatar={<Logo src={AvatarImg} />} />}
          headerType={headerType}
          searchComponent={<NavbarSearch />}
          location={router}
          searchVisibility={searchVisibility}
        />
        <MobileNavbar className={headerType}>
          <LogoArea>
            <>
              {headerType === 'transparent'
                ? <><img src={eventsLogoImgWhite} className="white-logo" /><img src={eventsLogoImgBlack} className="black-logo" /></>
                : <Logo withLink linkTo="/" src={eventsLogoImgBlack} className="black-logo" title="Events" />}
            </>
            <NavbarSearch />
          </LogoArea>
          <Button
            className={`hamburg-btn ${state ? 'active' : ''}`}
            onClick={sidebarHandler}
          >
            <span />
            <span />
            <span />
          </Button>
          <Drawer
            placement="right"
            closable={false}
            onClose={sidebarHandler}
            width="285px"
            className="mobile-header"
            visible={state}
          >
            <CloseDrawer>
              <button onClick={sidebarHandler}>
                <IoIosClose />
              </button>
            </CloseDrawer>
            {loggedIn ? (
              <AvatarWrapper>
                <AvatarImage>
                  <Logo src={AvatarImg} />
                </AvatarImage>
                <AvatarInfo>
                  <Text as="h3" content="" />
                  <Link href={USER_PROFILE_PAGE}>
                    <a>{t('user.profile')}</a>
                  </Link>
                </AvatarInfo>
              </AvatarWrapper>
            ) : (
                <AuthMenu className="auth-menu" />
              )}
            <MobileMenu t={t} className="main-menu" />
          </Drawer>
        </MobileNavbar>
      </Sticky>
    </HeaderWrapper>
  );
};

Header.defaultProps = {
  i18nNamespaces: ['mainMenu']
}

Header.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('mainMenu')(withRouter(Header))