import React from 'react';
import { withRouter } from 'next/router';
import { Menu } from 'antd';
import PropTypes from 'prop-types'
import ActiveLink from 'library/helpers/activeLink';
import { withTranslation } from 'i18n'

import {
  HOME_PAGE,
  LISTING_POSTS_PAGE,
  EVENTS_LIST_PAGE,
  USER_PROFILE_PAGE,
  PRICING_PLAN_PAGE,
  ADD_EVENT_PAGE
} from 'settings/constant';

const MainMenu = ({ t, className, router }) => {
  return (
    <Menu className={className}>
      <Menu.Item key="0">
        <ActiveLink className={router.pathname === HOME_PAGE ? 'active' : ''} href={`${HOME_PAGE}`}
        >
          {t('mainPage')}
        </ActiveLink>
      </Menu.Item>
      <Menu.Item key="1">
        <ActiveLink
          className={router.pathname === EVENTS_LIST_PAGE ? 'active' : ''}
          href={`${EVENTS_LIST_PAGE}`}
        >
          {t('events')}
        </ActiveLink>
      </Menu.Item>
      <Menu.Item key="2">
        <ActiveLink className={router.pathname === ADD_EVENT_PAGE ? 'active' : ''} href={`${ADD_EVENT_PAGE}`}
        >
          {t('createEvent')}
        </ActiveLink>
      </Menu.Item>
    </Menu>
  );
};

MainMenu.defaultProps = {
  i18nNamespaces: ['mainMenu']
}

MainMenu.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('mainMenu')(withRouter(MainMenu))
