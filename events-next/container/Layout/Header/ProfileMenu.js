import React, { useContext, useState, useRef } from 'react';
import { Menu } from 'antd';
import useOnClickOutside from 'library/hooks/useOnClickOutside';
import ActiveLink from 'library/helpers/activeLink';
import { AuthContext } from 'context/AuthProvider';
import { useTranslation } from 'i18n'
import {
  USER_PROFILE_PAGE,
  USER_ACCOUNT_SETTINGS_PAGE,
  ADD_EVENT_PAGE
} from 'settings/constant';

const ProfileMenu = ({ avatar }) => {
  const { logOut } = useContext(AuthContext);
  const [state, setState] = useState(false);
  const { t } = useTranslation('mainMenu')

  const handleDropdown = () => {
    setState(!state);
  };

  const closeDropdown = () => {
    setState(false);
  };

  const dropdownRef = useRef(null);
  useOnClickOutside(dropdownRef, () => setState(false));

  return (
    <div className="avatar-dropdown" ref={dropdownRef}>
      <div className="dropdown-handler" onClick={handleDropdown}>
        {avatar}
      </div>

      <Menu className={`dropdown-menu ${state ? 'active' : 'hide'}`}>
        <Menu.Item onClick={closeDropdown} key="0">
          <ActiveLink href={USER_PROFILE_PAGE}>{t('user.profile')}</ActiveLink>
        </Menu.Item>
        <Menu.Item onClick={closeDropdown} key="1">
          <ActiveLink href={ADD_EVENT_PAGE}>{t('createEvent')}</ActiveLink>
        </Menu.Item>
        <Menu.Item onClick={closeDropdown} key="2">
          <ActiveLink href={USER_ACCOUNT_SETTINGS_PAGE}>
          {t('user.settings')}
          </ActiveLink>
        </Menu.Item>
        <Menu.Item key="3">
          <button onClick={logOut}>{t('logout')}</button>
        </Menu.Item>
      </Menu>
    </div>
  );
};

export default ProfileMenu;
