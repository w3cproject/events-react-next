import React from 'react';
import PropTypes from 'prop-types'
import { Menu } from 'antd';
import { LOGIN_PAGE } from 'settings/constant';
import { withTranslation, Link } from 'i18n'

const AuthMenu = ({ t, className }) => {
  return (
    <Menu className={className}>
      <Menu.Item key="0">
        <Link href={`${LOGIN_PAGE}`}>
          <a>{t('login')}</a>
        </Link>
      </Menu.Item>
      {/* <Menu.Item key="1">
        <Link href={`${REGISTRATION_PAGE}`}>
          <a>{t('signUp')}</a>
        </Link>
      </Menu.Item> */}
    </Menu>
  );
};

AuthMenu.defaultProps = {
  i18nNamespaces: ['mainMenu']
}

AuthMenu.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('mainMenu')(AuthMenu)
