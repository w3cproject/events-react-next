import React, { useContext } from 'react';
import { Menu } from 'antd';
import ActiveLink from 'library/helpers/activeLink';
import { AuthContext } from 'context/AuthProvider';
import {
  HOME_PAGE,
  LISTING_POSTS_PAGE,
  PRICING_PLAN_PAGE,
  EVENTS_LIST_PAGE,
  USER_PROFILE_PAGE,
  USER_ACCOUNT_SETTINGS_PAGE,
} from 'settings/constant';

const MobileMenu = ({ t, className }) => {
  // auth context
  const { loggedIn, logOut } = useContext(AuthContext);

  return (
    <Menu className={className}>
      <Menu.Item key="0">
        <ActiveLink href={EVENTS_LIST_PAGE}>{t('events')}</ActiveLink>
      </Menu.Item>
      {loggedIn && (
        <>
          <Menu.Item key="1">
            <ActiveLink href={USER_PROFILE_PAGE}>
              {t('user.profile')}
            </ActiveLink>
          </Menu.Item>
          <Menu.Item key="2">
            <button onClick={logOut}>{t('logout')}</button>
          </Menu.Item>
        </>
      )}
    </Menu>
  );
};

export default MobileMenu;
