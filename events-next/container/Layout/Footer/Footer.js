import React from 'react';
import Logo from 'components/UI/Logo/Logo';
import Footers from 'components/Footer/Footer';
import LogoImage from 'assets/images/events_logo_black.svg';
import FooterMenu from './FooterMenu';
import PropTypes from 'prop-types'
import { useTranslation } from 'i18n'

const Footer = ({ path }) => {
  const { t } = useTranslation('mainMenu')

  return (
    <Footers
      path={path}
      logo={<Logo withLink linkTo="/" src={LogoImage} title="Events" />}
      menu={<FooterMenu />}
      copyright={`Copyright @ ${new Date().getFullYear()} Events Global Solutions, Inc.`}
    />
  );
};

export default Footer
