import React from 'react';
import { Link } from 'i18n';
import { Menu } from 'antd';
import { useTranslation } from 'i18n'

import {
    HOME_PAGE,
    EVENTS_LIST_PAGE,
    PRIVACY_PAGE,
    PRICING_PLAN_PAGE,
    USER_PROFILE_PAGE,
    TERMS_PAGE
} from '../../../settings/constant';
import {withTranslation} from "react-i18next";
import PropTypes from 'prop-types'

const FooterMenu = ({ t, className, router }) => {
    return (
        <Menu>
            <Menu.Item key="0">
                <Link href={`${HOME_PAGE}`}>
                    <a>{t('mainPage')}</a>
                </Link>
            </Menu.Item>
            <Menu.Item key="1">
                <Link href={`${EVENTS_LIST_PAGE}`}>
                    <a>{t('events')}</a>
                </Link>
            </Menu.Item>
            <Menu.Item key="2">
                <a href={PRIVACY_PAGE}>{t('privacy')}</a>
            </Menu.Item>
            <Menu.Item key="3">
                <a href={TERMS_PAGE}>{t('termsOfUse')}</a>
            </Menu.Item>
        </Menu>
    );
};

FooterMenu.defaultProps = {
    i18nNamespaces: ['mainMenu']
};

FooterMenu.propTypes = {
    t: PropTypes.func.isRequired,
};

export default withTranslation('mainMenu')(FooterMenu)
