import React from 'react';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import { EVENTS_LIST_PAGE } from 'settings/constant';
import {
  LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP,
  EVENTS_LIST_PAGE_LIMIT
} from 'settings/config';

export default function AgentFavItemLists({
  userFavorites,
  loadMoreData,
  loading,
  deviceType,
}) {
  const favourite_post = userFavorites ?? [];

  if (favourite_post.length > 0) {
    return (
      <SectionGrid
        link={EVENTS_LIST_PAGE}
        columnWidth={LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP}
        deviceType={deviceType}
        data={favourite_post}
        totalItem={favourite_post.length}
        limit={EVENTS_LIST_PAGE_LIMIT}
        loading={loading}
        handleLoadMore={loadMoreData}
        placeholder={<PostPlaceholder />}
      />
    )
  }

  return (
    <div>
      <p>У вас пока нет избранных мероприятий</p>
    </div>
  );
}
