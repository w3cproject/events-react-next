import React, { useState, Fragment } from 'react';
import { Link } from 'i18n';
import dynamic from 'next/dynamic';
import isEmpty from 'lodash/isEmpty';
import { Menu, Popover } from 'antd';
import Container from 'components/UI/Container/Container';
import Image from 'components/UI/Image/Image';
import Heading from 'components/UI/Heading/Heading';
import Text from 'components/UI/Text/Text';
import { ProfilePicLoader } from 'components/UI/ContentLoader/ContentLoader';
import Loader from 'components/Loader/Loader';
import { ADD_EVENT_PAGE, COMPANY_OWNER_PAGE } from 'settings/constant';
import AgentDetailsPage, {
  BannerSection,
  UserInfoArea,
  ProfileImage,
  ProfileInformationArea,
  ProfileInformation,
  SocialAccount,
  NavigationArea,
} from './AgentDetails.style';
import {
  IoLogoTwitter,
  IoLogoFacebook,
  IoLogoInstagram,
  IoIosAdd,
} from 'react-icons/io';
import bannerCover from 'assets/images/banner/17.jpg'
import { last } from 'lodash';

const AgentFavItemLists = dynamic(() => import('./AgentFavItemLists'));
const AgentContact = dynamic(() => import('./AgentContact'));
const AgentItemLists = dynamic(() => import('./AgentItemLists'));
const AgentPaymentsLists = dynamic(() => import('./AgentPaymentsLists'));

const ProfileNavigation = (props) => {
  const [component, setComponent] = useState('allListing');
  const { className, t, userProfile } = props;

  return (
    <>
      <NavigationArea>
        <Container fluid={true}>
          <Menu className={className}>
            <Menu.Item key="0">
              <a
                className={component === 'allListing' ? 'active' : ''}
                onClick={() => setComponent('allListing')}
              >
                {t('bookings')}
              </a>
            </Menu.Item>
            <Menu.Item key="1">
              <a
                className={component === 'favouriteListing' ? 'active' : ''}
                onClick={() => setComponent('favouriteListing')}
              >
                {t('favorites')}
              </a>
            </Menu.Item>
            <Menu.Item key="2">
              <a
                className={component === 'paymentsListing' ? 'active' : ''}
                onClick={() => setComponent('paymentsListing')}
              >
                {t('payments')}
              </a>
            </Menu.Item>
            {/* <Menu.Item key="2">
              <a
                className={component === 'contact' ? 'active' : ''}
                onClick={() => setComponent('contact')}
              >
                Contact
              </a>
            </Menu.Item> */}
          </Menu>

          <Link href={`${COMPANY_OWNER_PAGE}/${userProfile.id}`}>
            <a className="add_card">
              {t('ownerPage')}
            </a>
          </Link>
          {/* <div>
            <Link href={`${ADD_EVENT_PAGE}`}>
              <a className="add_card">
                <IoIosAdd /> {t('createEvent')}
              </a>
            </Link>
          </div> */}
        </Container>
      </NavigationArea>
      <Container fluid={true}>
        {component === 'allListing' && <AgentItemLists {...props} />}
        {component === 'favouriteListing' && <AgentFavItemLists {...props} />}
        {component === 'paymentsListing' && <AgentPaymentsLists {...props} />}
        {/* {component === 'contact' && <AgentContact {...props} />} */}
      </Container>
    </>
  );
};

const AgentProfileInfo = (props) => {
  const { userProfile, loading } = props;
  if (isEmpty(userProfile) || loading) return <Loader />;
  const {
    firstName,
    lastName,
    profilePicture,
    username,
    content,
    coverPicture,
    social_profile,
  } = userProfile

  const coverImage = coverPicture ?? bannerCover
  const fullName = (firstName) ? firstName + ' ' + lastName : username

  return (
    <Fragment>
      <BannerSection
        style={{
          background: `url(${coverImage}) center center / cover no-repeat`,
        }}
      />
      <UserInfoArea>
        <Container fluid={true}>
          <ProfileImage>
            {profilePicture ? (
              <Image src={`${process.env.AMAZON_ASSETS_URL}/${profilePicture} `} alt="Profile Pic" />
            ) : (
                <ProfilePicLoader />
              )}
          </ProfileImage>
          <ProfileInformationArea>
            <ProfileInformation>
              <Heading content={fullName} />
              <Text content={content} />
            </ProfileInformation>
            {/* <SocialAccount>
              <Popover content="Twitter">
                <a
                  href={social_profile.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <IoLogoTwitter className="twitter" />
                </a>
              </Popover>
              <Popover content="Facebook">
                <a
                  href={social_profile.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <IoLogoFacebook className="facebook" />
                </a>
              </Popover>
              <Popover content="Instagram">
                <a
                  href={social_profile.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <IoLogoInstagram className="instagram" />
                </a>
              </Popover>
            </SocialAccount> */}
          </ProfileInformationArea>
        </Container>
      </UserInfoArea>
    </Fragment>
  );
};

export default function AgentDetailsViewPage(props) {
  return (
    <AgentDetailsPage>
      {/* <AuthProvider> */}
      <AgentProfileInfo {...props} />
      <ProfileNavigation {...props} />
      {/* </AuthProvider> */}
    </AgentDetailsPage>
  );
}
