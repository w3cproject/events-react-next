import React from 'react';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import { EVENTS_LIST_PAGE } from 'settings/constant';
import { Table } from 'antd'
import {
  LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP,
  EVENTS_LIST_PAGE_LIMIT
} from 'settings/config';
import NumberFormat from 'react-number-format'
import { Link } from 'i18n'
import moment from 'moment'

export default function AgentItemLists({
  userPayments,
  loadMoreData,
  loading,
  deviceType,
}) {
  const listed_payments = userPayments ?? [];
  const columns = [
    {
      title: 'Сумма',
      dataIndex: 'amount',
      key: 'amount',
    },
    {
      title: 'Мероприятие',
      dataIndex: 'event',
      key: 'event',
      render: text => <Link href={`/events/${text}`}><a target="_blank">{text}</a></Link>,
    },
    {
      title: 'ID платежа',
      dataIndex: 'paymentId',
      key: 'paymentId',
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Дата',
      dataIndex: 'date',
      key: 'date',
    },
  ]

  const data = []

  for (let i = 0; i < listed_payments.length; i++) {
    data.push({
      key: i,
      amount: <NumberFormat value={listed_payments[i].amount}
        displayType={'text'}
        suffix={`${listed_payments[i].currency === 'rub' ? '₽' : '$'}`}
        thousandSeparator={true} />,
      event: listed_payments[i].eventId,
      paymentId: listed_payments[i].id,
      status: listed_payments[i].status,
      date: moment(listed_payments[i].createdAt).format('MM.DD.Y HH:mm')
    })
  }

  return (
    <div>
      <Table columns={columns} dataSource={data} />
    </div>
  );
}
