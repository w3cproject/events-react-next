import React from 'react';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import { EVENTS_LIST_PAGE } from 'settings/constant';
import {
  LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP,
  EVENTS_LIST_PAGE_LIMIT
} from 'settings/config';

export default function AgentItemLists({
  userBookings,
  loadMoreData,
  loading,
  deviceType,
}) {
  const listed_post = userBookings ?? [];

  return (
    <SectionGrid
      link={EVENTS_LIST_PAGE}
      columnWidth={LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP}
      deviceType={deviceType}
      data={listed_post}
      totalItem={listed_post.length}
      limit={EVENTS_LIST_PAGE_LIMIT}
      loading={loading}
      handleLoadMore={loadMoreData}
      placeholder={<PostPlaceholder />}
    />
  );
}
