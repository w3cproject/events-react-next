import React, { useState } from 'react';
import dynamic from 'next/dynamic';
import ActiveLink from 'library/helpers/activeLink';
import { Row, Col, Menu, Avatar, Affix } from 'antd';
import Container from 'components/UI/Container/Container.style';
import { USER_PROFILE_PAGE } from 'settings/constant';
import { UserOutlined } from '@ant-design/icons';
import AccountSettingWrapper, {
  AccountSidebar,
  AgentAvatar,
  SidebarMenuWrapper,
  ContentWrapper,
  AgentName,
  FromWrapper,
} from './AccountSettings.style';

const AgentCreateOrUpdateForm = dynamic(() =>
  import('./AgentCreateOrUpdateForm')
);
const AgentPictureChangeForm = dynamic(() =>
  import('./AgentPictureChangeForm')
);
const ChangePassWord = dynamic(() => import('./ChangePassWordForm'));

export default function AgentAccountSettingsPage(props) {
  const { processedData, user } = props;
  const [currentRoute, setCurrentRoute] = useState('edit-profile');
  const [container, setContainer] = useState(null);
  const userName = processedData.firstName
    ? processedData.firstName + ' ' + processedData.lastName
    : processedData.username;
  const profilePic = processedData.profilePicture;
  return (
    <AccountSettingWrapper>
      <Container fullWidth={true}>
        <Row gutter={30}>
          <Col md={9} lg={6} ref={setContainer}>
            <Affix target={() => container}>
              <AccountSidebar>
                <AgentAvatar>
                  {profilePic
                    ? <Avatar src={process.env.AMAZON_ASSETS_URL + '/' + profilePic} alt="Profile Picture" />
                    : <Avatar size={90} icon={<UserOutlined />} alt="Profile Picture" style={{ color: '#f56a00', backgroundColor: '#fde3cf' }} />}
                  <ContentWrapper>
                    <AgentName>
                      {userName}
                    </AgentName>
                    <ActiveLink href={`${USER_PROFILE_PAGE}`}>
                      View profile
                  </ActiveLink>
                  </ContentWrapper>
                </AgentAvatar>
                <>
                  <SidebarMenuWrapper>
                    <Menu
                      defaultSelectedKeys={['1']}
                      defaultOpenKeys={['sub1']}
                      mode="inline"
                    >
                      <Menu.Item key="1">
                        <a onClick={() => setCurrentRoute('edit-profile')}>
                          Edit Profile
                      </a>
                      </Menu.Item>
                      <Menu.Item key="2">
                        <a onClick={() => setCurrentRoute('change-photo')}>
                          Change Photos
                      </a>
                      </Menu.Item>
                      <Menu.Item key="3">
                        <a onClick={() => setCurrentRoute('change-password')}>
                          Change Password
                      </a>
                      </Menu.Item>
                    </Menu>
                  </SidebarMenuWrapper>
                </>
              </AccountSidebar>
            </Affix>
          </Col>
          <Col md={15} lg={18}>
            <FromWrapper>
              {currentRoute === 'edit-profile' ? (
                <AgentCreateOrUpdateForm processedData={processedData} user={user} />
              ) : (
                  ''
                )}
              {currentRoute === 'change-photo' ? (
                <AgentPictureChangeForm />
              ) : (
                  ''
                )}
              {currentRoute === 'change-password' ? <ChangePassWord /> : ''}
            </FromWrapper>
          </Col>
        </Row>
      </Container>
    </AccountSettingWrapper>
  );
}
