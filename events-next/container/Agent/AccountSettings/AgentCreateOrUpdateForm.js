import React, { useState, useContext, Fragment } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, Select, Button, Spin, message } from 'antd';
import FormControl from 'components/UI/FormControl/FormControl';
import DatePicker from 'components/UI/AntdDatePicker/AntdDatePicker';
import { FormTitle } from './AccountSettings.style';
import { AuthContext } from 'context/AuthProvider';
import moment from 'moment'

const dateFormat = 'YYYY/MM/DD';

const genderOptions = [
  { label: 'Male', value: 'male' },
  { label: 'Female', value: 'female' },
  { label: 'Other', value: 'Other' },
];
const languageOptions = [
  { label: 'English', value: 'english' },
  { label: 'Spanish', value: 'spanish' },
  { label: 'French', value: 'french' },
  { label: 'Russian', value: 'russian' },
];

export default function UserCreateOrUpdateForm(props) {
  const { processedData, user } = props
  const { control, errors, handleSubmit } = useForm();
  const [formSubmitState, setFormSubmitState] = useState(false)
  const onSubmit = async (data) => {
    setFormSubmitState(true)
    const req = await fetch(`${process.env.API_URL}/users/${user.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${user.accessToken}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    const res = await req.json()
    setFormSubmitState(false)

    message.success({
      content: 'Profile successful saved',
      className: 'custom-class',
      style: {
        // marginTop: '20vh'
      }
    });
  }

  function disabledDate(current) {
    // Can not select days before today and today
    return current > moment().month(0).day(0).subtract(16, 'years')
  }
  return (
    <Fragment>
      <FormTitle>Basic Information</FormTitle>
      <Spin spinning={formSubmitState}>
        <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
          <Row gutter={30}>
            <Col lg={6}>
              <FormControl
                label="First name"
                htmlFor="firstName"
                error={errors.firstName && <span>This field is required!</span>}
              >
                <Controller
                  as={<Input />}
                  id="firstName"
                  name="firstName"
                  defaultValue={processedData.firstName}
                  control={control}
                  rules={{ required: true }}
                />
              </FormControl>
            </Col>
            <Col lg={6}>
              <FormControl
                label="Last name"
                htmlFor="lastName"
                error={errors.lastName && <span>This field is required!</span>}
              >
                <Controller
                  as={<Input />}
                  id="lastName"
                  name="lastName"
                  defaultValue={processedData.lastName}
                  control={control}
                  rules={{ required: true }}
                />
              </FormControl>
            </Col>
            <Col md={6}>
              <FormControl
                label="I am"
                htmlFor="agentGender"
                error={
                  errors.agentGender && <span>This field is required!</span>
                }
              >
                <Controller
                  as={<Select options={genderOptions} />}
                  id="agentGender"
                  name="agentGender"
                  defaultValue={'male'}
                  control={control}
                  rules={{ required: true }}
                />
              </FormControl>
            </Col>
            <Col md={6}>
              <FormControl
                label="Preferred Language"
                htmlFor="preferredLanguage"
                error={
                  errors.preferredLanguage && (
                    <span>This field is required!</span>
                  )
                }
              >
                <Controller
                  as={<Select options={languageOptions} />}
                  id="preferredLanguage"
                  name="preferredLanguage"
                  defaultValue={'english'}
                  control={control}
                  rules={{ required: true }}
                />
              </FormControl>
            </Col>
          </Row>
          <Row gutter={30}>
            {/* <Col lg={12}>
              <FormControl
                label="Date of birth"
                htmlFor="birthdayDate"
                error={
                  errors.birthdayDate && <span>This field is required!</span>
                }
              >
                <Controller
                  as={<DatePicker mode="date" disabledDate={disabledDate} />}
                  id="birthdayDate"
                  name="birthdayDate"
                  // defaultValue={processedData.birthdayDate ? moment(processedData.birthdayDate, 'YYYY-MM-DD') : ''}
                  control={control}
                  rules={{ required: false }}
                />
              </FormControl>
            </Col> */}
            <Col lg={24}>
              <Row gutter={30}>
                <Col lg={6}>
                  <FormControl
                    label="Email address"
                    htmlFor="email"
                    error={
                      errors.email && (
                        <>
                          {errors.email?.type === 'required' && (
                            <span>This field is required!</span>
                          )}
                          {errors.email?.type === 'pattern' && (
                            <span>Please enter a valid email address!</span>
                          )}
                        </>
                      )
                    }
                  >
                    <Controller
                      as={<Input readOnly={true} />}
                      type="email"
                      id="email"
                      name="email"
                      defaultValue={processedData.email}
                      control={control}
                      rules={{
                        required: true,
                        pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                      }}
                    />
                  </FormControl>
                </Col>
                <Col lg={6}>
                  <FormControl
                    label="Phone number"
                    htmlFor="phoneNumber"
                    error={
                      errors.phoneNumber && (
                        <>
                          {errors.phoneNumber?.type === 'required' && (
                            <span>This field is required!</span>
                          )}
                          {errors.phoneNumber?.type === 'pattern' && (
                            <span>Please enter your valid number!</span>
                          )}
                        </>
                      )
                    }
                  >
                    <Controller
                      as={<Input />}
                      id="phoneNumber"
                      name="phoneNumber"
                      type="phone"
                      defaultValue={processedData.phone}
                      control={control}
                      rules={{
                        required: true
                      }}
                    />
                  </FormControl>
                </Col>
                <Col lg={12}>
                  <FormControl
                    label="Profile status (Optional)"
                    htmlFor="profileLine"
                    error={errors.profileLine && <span>This field is required!</span>}
                  >
                    <Controller
                      as={<Input />}
                      id="profileLine"
                      name="profileLine"
                      defaultValue={processedData.profileLine}
                      control={control}
                      rules={{ required: false }}
                    />
                  </FormControl>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row gutter={30}>
            <Col lg={24}>
              <FormControl
                label="Describe Yourself (Optional)"
                htmlFor="about"
              >
                <Controller
                  as={<Input.TextArea rows={5} />}
                  id="about"
                  name="about"
                  defaultValue={processedData.about}
                  control={control}
                />
              </FormControl>
            </Col>
          </Row>
          <div className="submit-container">
            <Button htmlType="submit" type="primary">
              Save Changes
          </Button>
          </div>
        </form>
      </Spin>
    </Fragment>
  );
};