import React, { useState, useEffect } from 'react';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Button, Select } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import { IoIosArrowBack } from 'react-icons/io';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { LANGUAGES_LIST } from 'settings/languages-constants'
import { i18n } from 'i18n'

const { Option } = Select

const CategoryInformation = ({ t, setStep, categories }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { control, register, errors, setValue, handleSubmit } = useForm();

  const onSubmit = (data) => {
    console.log(data)
    action(data);
    setStep(6);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step5Header')}</Title>
        </FormHeader>
        <Row gutter={30}>
          <Col sm={12} offset={6}>
            <FormControl
              label={t('categories')}
              htmlFor="categories"
              error={errors.categories && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Select mode="multiple"
                  allowClear
                  style={{ width: '100%' }}
                  placeholder="Please select">
                  {categories.map((category, index) => (
                    <Option key={index} value={category.id}>
                      {i18n.language === 'ru'
                        ? category.title
                        : category.title_en}
                    </Option>
                  ))}
                </Select>}
                id="categories"
                name="categories"
                defaultValue={state.data.categories}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="submit"
            onClick={() => setStep(4)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default CategoryInformation;
