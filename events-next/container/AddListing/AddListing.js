import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { StateMachineProvider, createStore } from 'little-state-machine';
import { Progress } from 'antd';
import BasicInformation from './BasicInformation';
import PreBasicInformation from './PreBasicInformation';
import TimeInformation from './TimeInformation';
import ImagesInformation from './ImagesInformation';
import HotelLocation from './HotelLocation';
import CategoryInformation from './CategoryInformation';
import AdditionalInformation from './AdditionalInformation';
import PriceInformation from './PriceInformation';
import PlaceInformation from './PlaceInformation';
import Stepper from './AddListing.style';

createStore({
  data: {},
});

const AddListing = ({ t, user, categories }) => {
  let stepComponent;
  const [step, setStep] = useState(1);
  switch (step) {
    case 1:
      stepComponent = <PreBasicInformation t={t} setStep={setStep} />;
      break;

    case 2:
      stepComponent = <BasicInformation t={t} setStep={setStep} />;
      break;

    case 3:
      stepComponent = <TimeInformation t={t} setStep={setStep} />;
      break;

    case 4:
      stepComponent = <ImagesInformation t={t} setStep={setStep} />;
      break;

    case 5:
      stepComponent = <CategoryInformation t={t} setStep={setStep} categories={categories} />;
      break;

    case 6:
      stepComponent = <PriceInformation t={t} setStep={setStep} />;
      break;
    // case 7:
    //   stepComponent = <PlaceInformation t={t} user={user} setStep={setStep} />;
    //   break;
    case 7:
      stepComponent = <AdditionalInformation t={t} user={user} setStep={setStep} />;
      break;

    default:
      stepComponent = null;
  }

  return (
    <StateMachineProvider>
      <Stepper>
        <Progress
          className="stepper-progress"
          percent={step * 12.5}
          strokeColor={{
            from: '#108ee9',
            to: '#87d068',
          }}
          showInfo={false}
          status="active"
        />
        {stepComponent}
      </Stepper>
    </StateMachineProvider>
  );
};

AddListing.propTypes = {
  categories: PropTypes.array
};

export default AddListing;
