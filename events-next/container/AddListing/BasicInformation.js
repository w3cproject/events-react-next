import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic'
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Button, Select } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import { IoIosArrowBack } from 'react-icons/io';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { LANGUAGES_LIST } from 'settings/languages-constants'
import { i18n } from 'i18n'
import DatePicker from 'components/UI/AntdDatePicker/AntdDatePicker'
import { EditorState, ContentState, convertToRaw, convertFromRaw, convertFromHTML } from 'draft-js'
import draftToHtml from 'draftjs-to-html';
import styled from 'styled-components';

const { Option } = Select

const DraftComponent = dynamic(
  () => import('react-draft-wysiwyg').then((mod) => mod.Editor),
  { ssr: false }
)

const BasicInformation = ({ t, setStep }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { control, register, errors, setValue, handleSubmit } = useForm();

  let defaultDescState = EditorState.createEmpty()

  const [descState, setDescState] = useState(EditorState.createEmpty())

  useEffect(() => {
    register({ name: 'detailDesc' }, { required: false })
    if (state.data.eventDescription) {
      const contentState = convertFromRaw(state.data.eventDescription);
      const editorState = EditorState.createWithContent(contentState);
      setDescState(editorState)
      setValue('eventDescription', state.data.eventDescription)
    }
    return () => { }
  }, [])

  const edidorToHtml = (draftState) => {
    return draftToHtml(convertToRaw(draftState.getCurrentContent()))
  }

  function onDesc(val) {
    setDescState(val)
    setValue('eventDescription', val)
    setValue('detailDesc', edidorToHtml(descState))
  }

  const onSubmit = (data) => {
    action(data);
    // console.log(state.data)
    setStep(3);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step2Header')}</Title>
        </FormHeader>
        <Row gutter={30}>
          <Col sm={12}>
            <FormControl
              label={t('title')}
              htmlFor="eventTitle"
              error={errors.eventTitle && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Input />}
                id="eventTitle"
                name="eventTitle"
                defaultValue={state.data.eventTitle}
                control={control}
                placeholder={t('titlePlaceholder')}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          <Col sm={12}>
            <FormControl
              label={t('primaryLanguage')}
              htmlFor="language"
              error={errors.language && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Select defaultValue={i18n.language}>
                  {LANGUAGES_LIST.map((lang, index) => (
                    <Option key={index} value={lang.value}>{t(`languages.${lang.value}`)}</Option>
                  ))}
                </Select>}
                id="language"
                name="language"
                defaultValue={state.data.language || i18n.language}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          {/* <Col sm={12}>
            <FormControl
              label="Category"
              htmlFor="category"
              error={errors.category && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Select defaultValue={i18n.category}>
                  {categories.map((category, index) => (
                    <Option key={index} value={category.id}>{category.title}</Option>
                  ))}
                </Select>}
                id="category"
                name="category"
                defaultValue={state.data.category}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col> */}
          {/* <Col sm={12}>
            <FormControl
              label="Price Per Night (USD)"
              htmlFor="pricePerNight"
              error={
                errors.pricePerNight && (
                  <>
                    {errors.pricePerNight?.type === 'required' && (
                      <span>{t('requiredField')}</span>
                    )}
                    {errors.pricePerNight?.type === 'pattern' && (
                      <span>Please enter only number!</span>
                    )}
                  </>
                )
              }
            >
              <Controller
                as={<InputNumber />}
                id="pricePerNight"
                name="pricePerNight"
                defaultValue={state.data.pricePerNight}
                control={control}
                placeholder="00.00"
                rules={{
                  required: true,
                  pattern: /^[0-9]*$/,
                }}
              />
            </FormControl>
          </Col> */}
        </Row>
        <FormControl
          label={t('description')}
          htmlFor="eventDescription"
          error={
            errors.eventDescription && <span>{t('requiredField')}</span>
          }
        >
          <Controller
            as={<DraftComponent
              toolbar={{
                options: ['inline', 'blockType', 'list', 'textAlign', 'link', 'emoji', 'remove', 'history'],
                inline: {
                  options: ['bold', 'italic', 'underline']
                },
              }}
              editorState={descState}
              toolbarClassName="toolbarClassName"
              wrapperClassName="wrapperClassName"
              editorClassName="editorClassName"
              onEditorStateChange={onDesc}
              localization={{
                locale: i18n.language,
              }}
            />}
            id="eventDescription"
            name="eventDescription"
            // defaultValue={draftToHtml(convertToRaw(descState.getCurrentContent()))}
            control={control}
            placeholder={t('descriptionPlaceholder')}
            rules={{
              required: true,
            }}
          />
          {/* <textarea
            disabled
            value={draftToHtml(convertToRaw(state.data.eventDescription.getCurrentContent()))}
          /> */}
        </FormControl>
        {/* <FormControl
          label="How many guests can your hotel accommodate?"
          error={errors.guest && <span>{t('requiredField')}</span>}
        >
          <InputIncDec
            name="guest"
            value={quantity.guest}
            onChange={handleOnChange('guest')}
            increment={() => handleIncrement('guest')}
            decrement={() => handleDecrement('guest')}
          />
        </FormControl>
        <FormControl
          label="How many beds can guests use?"
          error={errors.bed && <span>{t('requiredField')}</span>}
        >
          <InputIncDec
            name="bed"
            value={quantity.bed}
            onChange={handleOnChange('bed')}
            increment={() => handleIncrement('bed')}
            decrement={() => handleDecrement('bed')}
          />
        </FormControl> */}
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="button"
            onClick={() => setStep(1)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

BasicInformation.propTypes = {
  categories: PropTypes.array
};


export default BasicInformation