import React, { useState, useEffect } from 'react';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Button, Select } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { LANGUAGES_LIST } from 'settings/languages-constants'
import { i18n } from 'i18n'
import CardButton from 'components/UI/CardButton/CardButton'
import { FaGlobe, FaDesktop, FaUsers } from 'react-icons/fa'

const PreBasicInformation = ({ t, setStep }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { handleSubmit } = useForm()
  const [isOnline, setIsOnline] = useState(true)

  const handleEventType = (value) => {
    state.data.eventType = value

    if (value === 'offline') {
      setIsOnline(false)
    } else {
      setIsOnline(true)
    }
  }

  const onSubmit = (data) => {
    action({ eventType: isOnline ? 'online' : 'offline' })
    setStep(2);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('whichEventType')}</Title>
          {/* <p>Online experiences can be hosted from anywhere through video, and in-person experiences are hosted on location</p> */}
        </FormHeader>
        <Row gutter={30} className="cards-event-type">
          <Col md={12} sm={24} align="right">
            <div onClick={() => handleEventType('online')}>
              <CardButton faIcon={<FaDesktop size={30} />} title={t('online')} content={t('onlineDesc')} className={isOnline ? 'selected' : ''} />
            </div>
          </Col>
          <Col md={12} sm={24}>
            <div onClick={() => handleEventType('offline')}>
              <CardButton faIcon={<FaUsers size={30} />} title={t('offline')} content={t('offlineDesc')} className={!isOnline ? 'selected' : ''} />
            </div>
          </Col>
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper" style={{ justifyContent: "flex-end" }}>
          <Button type="primary" htmlType="submit">
          {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default PreBasicInformation;
