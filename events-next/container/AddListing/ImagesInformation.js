import React, { useEffect } from 'react';
import { IoIosArrowBack } from 'react-icons/io';
import { useStateMachine } from 'little-state-machine';
import { useForm } from 'react-hook-form';
import { Button, message } from 'antd';
import DragAndDropUploader from 'components/UI/ImageUploader/DragAndDropUploader';
import DragAndDropCropUploader from 'components/UI/ImageUploader/DragAndDropCropUploader';
import FormControl from 'components/UI/FormControl/FormControl';
import AddListingAction from './AddListingAction';
import {
  FormHeader,
  Title,
  FormContent,
  FormAction,
  ImagesFormContainer,
  ImagesFieldWrapper,
  ImagesFieldPreviewVertical,
  ImagesFieldPreviewHorizontal
} from './AddListing.style';

const EventImages = ({ t, setStep }) => {
  const { register, errors, setValue, handleSubmit } = useForm({});

  const { action, state } = useStateMachine(AddListingAction);

  useEffect(() => {
    register({ name: 'coverPicture' }, { required: false });
    register({ name: 'previewPicture' }, { required: true });
  }, [register]);

  const onSubmit = (data) => {
    action(data);
    setStep(5);
  };

  const onUploadPreview = (data) => {
    setValue('previewPicture', null)
    setValue('previewPicture', data)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step4Header')}</Title>
        </FormHeader>
        <h2>{t('coverPicture')}</h2>
        <p>{t('coverSize')}</p>
        <ImagesFormContainer>
          <FormControl
            error={errors.coverPicture && <span>{t('requiredField')}</span>}
          >
            <ImagesFieldPreviewHorizontal>
              <DragAndDropUploader
                name="coverPicture"
                value={state.data.coverPicture}
                onUploadChange={(data) => setValue('coverPicture', data)}
                imageExist={state.data.coverPicture}
              />
            </ImagesFieldPreviewHorizontal>
          </FormControl>
          <h2>{t('previewPicture')}</h2>
          <p>{t('previewSize')}</p>
          <FormControl
            error={errors.previewPicture && message.error(t('previewPictureError'))}
          >
            <ImagesFieldPreviewVertical>
              <DragAndDropCropUploader
                name="previewPicture"
                value={state.data.previewPicture}
                onUploadChange={(data) => onUploadPreview(data)}
                imageExist={state.data.previewPicture}
              />
            </ImagesFieldPreviewVertical>
          </FormControl>
        </ImagesFormContainer>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="button"
            onClick={() => setStep(3)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default EventImages;
