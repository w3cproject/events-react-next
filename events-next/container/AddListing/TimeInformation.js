import React, { useState, useEffect } from 'react';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Button, Select, TimePicker } from 'antd';
import FormControl from 'components/UI/FormControl/FormControl';
import { IoIosArrowBack } from 'react-icons/io';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import DatePicker from 'components/UI/AntdDatePicker/AntdDatePicker'
import moment from 'moment'
import { i18n } from 'i18n'

const currentDateTime = new Date()

const dateTimeFormat = 'YYYY/MM/DD'
const timeFormat = 'HH:mm'

const disabledDate = (current) => {
  // Can not select days before today and today
  return current && current < moment().endOf('day');
}


const TImeInformation = ({ t, setStep }) => {
  const { action, state } = useStateMachine(AddListingAction)
  const { control, errors, handleSubmit } = useForm()

  const onSubmit = (data) => {
    data.duration = data.duration.format(timeFormat)
    data.startDate = data.startDate.format(dateTimeFormat) + ' ' + data.startTime.format(timeFormat)
    action(data);
    setStep(4);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step3Header')}</Title>
        </FormHeader>
        <Row gutter={30}>
          <Col sm={12}>
            <FormControl
              label={t('startDate')}
              htmlFor="startDate"
              error={errors.startDate && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<DatePicker defaultValue={state.data.startDate && moment(state.data.startDate, dateTimeFormat)}
                  locale={'ru_RU'}
                  disabledDate={disabledDate}
                  format={dateTimeFormat} />}
                id="startDate"
                name="startDate"
                defaultValue={state.data.startDate && moment(state.data.startDate, dateTimeFormat)}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          <Col sm={12}>
            <FormControl
              label={t('startTime')}
              htmlFor="startTime"
              error={errors.startTime && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<TimePicker defaultValue={state.data.startTime && moment(state.data.startTime, timeFormat)}
                  format={timeFormat}
                  locale={i18n.language} />}
                id="startTime"
                name="startTime"
                defaultValue={state.data.startTime && moment(state.data.startTime, timeFormat)}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          <Col sm={12}>
            <FormControl
              label={t('duration')}
              htmlFor="duration"
              error={errors.duration && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<TimePicker defaultValue={state.data.duration && moment(state.data.duration, timeFormat)}
                  format={timeFormat}
                  locale={i18n.language} />}
                id="duration"
                name="duration"
                defaultValue={state.data.duration && moment(state.data.duration, timeFormat)}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="button"
            onClick={() => setStep(2)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default TImeInformation