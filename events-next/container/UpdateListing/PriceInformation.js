import React, { useState, useEffect } from 'react';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Checkbox, Button, Select } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import { IoIosArrowBack } from 'react-icons/io';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { CURRENCIES_LIST } from 'settings/currencies-constants'
import { i18n } from 'i18n'
import { langToCurrency, currencyToSign } from 'library/helpers/currency_helper.js'

const { Option } = Select

const CategoryInformation = ({ t, setStep }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { control, register, errors, setValue, handleSubmit } = useForm();
  const [eventIsFree, setEventIsFree] = useState(false)
  const [currencyChanger, setCurrencyChanger] = useState(currencyToSign(langToCurrency(i18n.language)))

  const onChangeCurrency = (currency) => {
    setCurrencyChanger(currencyToSign(currency))
  }

  const onChangeIsFree = (e) => {
    setEventIsFree(e[0].target.checked)
    state.data.price = 0
  }

  const onSubmit = (data) => {
    action(data);
    setStep(7);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step6Header')}</Title>
        </FormHeader>
        {!eventIsFree &&
          <Row gutter={30}>
            <Col sm={4} offset={8}>
              <FormControl
                label={t('price')}
                htmlFor="price"
              >
                <Controller
                  as={<InputNumber formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={value => value.replace(/\$\s?|(,*)/g, '')} style={{ maxWidth: '100%' }} />}
                  id="price"
                  name="price"
                  defaultValue={0}
                  control={control}
                />
              </FormControl>
            </Col>
            <Col sm={4}>
              <FormControl
                label={t('currency')}
                htmlFor="currency"
              >
                <Controller
                  as={<Select
                    style={{ width: '100%' }}
                    placeholder="Please select">
                    {CURRENCIES_LIST.map((currency, index) => (
                      <Option key={index} value={currency.title.toLowerCase()}>
                        {currency.value}
                      </Option>
                    ))}
                  </Select>}
                  id="currency"
                  name="currency"
                  // onChange={e => onChangeCurrency(e)}
                  defaultValue={state.data.currency ?? currencyToSign(i18n.language)}
                  control={control}
                />
              </FormControl>
            </Col>
          </Row>
        }
        <Row gutter={30}>
          <Col sm={12} offset={6}>
            <FormControl
              label={t('chooseIfFree')}
              htmlFor="priceFreeStatus"
            >
              <Controller
                as={<Checkbox>{t('free')}</Checkbox>}
                id="priceFreeStatus"
                name="priceFreeStatus"
                onChange={onChangeIsFree}
                checked={eventIsFree}
                // defaultValue={eventIsFree}
                control={control}
              />
            </FormControl>
          </Col>
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="submit"
            onClick={() => setStep(5)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default CategoryInformation;
