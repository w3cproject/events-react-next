import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Button, Select } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import { IoIosArrowBack } from 'react-icons/io';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { LANGUAGES_LIST } from 'settings/languages-constants'
import { i18n } from 'i18n'

const { Option } = Select

const BasicInformation = ({ t, setStep }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { control, register, errors, setValue, handleSubmit } = useForm();

  const onSubmit = (data) => {
    action(data);
    // console.log(state.data)
    setStep(8);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step7Header')}</Title>
        </FormHeader>
        <Row gutter={30}>
          <Col sm={12}>
            <FormControl
              label={t('title')}
              htmlFor="venueTitle"
              error={errors.venueTitle && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Input />}
                id="venueTitle"
                name="venueTitle"
                defaultValue={state.data.venueTitle}
                control={control}
                placeholder={t('titlePlaceholder')}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          <Col sm={12}>
            <FormControl
              label={t('primaryLanguage')}
              htmlFor="venueCity"
              error={errors.venueCity && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Select defaultValue={i18n.venueCity}>
                  {LANGUAGES_LIST.map((lang, index) => (
                    <Option key={index} value={lang.value}>{t(`venueCitys.${lang.value}`)}</Option>
                  ))}
                </Select>}
                id="venueCity"
                name="venueCity"
                defaultValue={state.data.venueCity}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col>
          {/* <Col sm={12}>
            <FormControl
              label="Category"
              htmlFor="category"
              error={errors.category && <span>{t('requiredField')}</span>}
            >
              <Controller
                as={<Select defaultValue={i18n.category}>
                  {categories.map((category, index) => (
                    <Option key={index} value={category.id}>{category.title}</Option>
                  ))}
                </Select>}
                id="category"
                name="category"
                defaultValue={state.data.category}
                control={control}
                rules={{
                  required: true
                }}
              />
            </FormControl>
          </Col> */}
          {/* <Col sm={12}>
            <FormControl
              label="Price Per Night (USD)"
              htmlFor="pricePerNight"
              error={
                errors.pricePerNight && (
                  <>
                    {errors.pricePerNight?.type === 'required' && (
                      <span>{t('requiredField')}</span>
                    )}
                    {errors.pricePerNight?.type === 'pattern' && (
                      <span>Please enter only number!</span>
                    )}
                  </>
                )
              }
            >
              <Controller
                as={<InputNumber />}
                id="pricePerNight"
                name="pricePerNight"
                defaultValue={state.data.pricePerNight}
                control={control}
                placeholder="00.00"
                rules={{
                  required: true,
                  pattern: /^[0-9]*$/,
                }}
              />
            </FormControl>
          </Col> */}
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            htmlType="button"
            onClick={() => setStep(6)}
          >
            <IoIosArrowBack /> {t('backBtn')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

BasicInformation.propTypes = {
  categories: PropTypes.array
};


export default BasicInformation