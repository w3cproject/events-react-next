import React, { useState } from 'react';
import { IoIosArrowBack } from 'react-icons/io';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Radio, Button, Spin, message } from 'antd';
import FormControl from 'components/UI/FormControl/FormControl';
import AddListingAction, { AddListingResetAction } from './AddListingAction';
import {
  FormHeader,
  Title,
  Description,
  FormContent,
  FormAction,
} from './AddListing.style';
// demo data
import { eventAdditionalData } from './EventAdditional.data';
import { Router } from 'i18n'

const AdditionalInformation = ({ t, user, setStep }) => {
  const { control, handleSubmit } = useForm();
  const { state } = useStateMachine(AddListingAction);
  const { action } = useStateMachine(AddListingResetAction);
  const [loader, setLoader] = useState(false)
  const onSubmit = async (data) => {
    setLoader(true)
    const formData = { ...state.data, ...data };
    console.log(formData)
    const req = await fetch(`${process.env.API_URL}/events`, {
      method: 'POST',
      headers: {
        'Authorization': 'Bearer ' + user.accessToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        title: formData.eventTitle,
        detailDesc: formData.detailDesc,
        startDate: formData.startDate,
        // startTime: formData.startTime,
        language: formData.primaryLanguage,
        eventTypes: formData.eventType,
        previewPicture: JSON.stringify(formData.previewPicture),
        coverPicture: JSON.stringify(formData.coverPicture),
        currency: formData.currency,
        price: formData.price,
        isPrivate: formData.isPrivate,
        categories: formData.categories,
        duration: formData.duration
      })
    })
    const res = await req.json()

    if (res.status === 'success') {
      action({});
      // window.STATE_MACHINE_RESET();
      Router.push(`/events/${res.eventId}`)
    } else {
      // message.error(res.message[Object.keys(res.message)[0]]);
      message.error(Object.keys(res.message)[0] + ': ' + Object.values(res.message)[0], 5)
    }

    setLoader(false)
  };

  return (
    <Spin spinning={loader}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormContent>
          <FormHeader>
            <Title>
              {t('step8Header')}
            </Title>
            {/* <Description>
            Add your hotel amenities, it can help travelers to choose their
            perfect hotel. Thanks.
          </Description> */}
          </FormHeader>
          <Row gutter={30}>
            {eventAdditionalData.map((item) => (
              <Col key={`hotel-amenities--key${item.id}`} md={8}>
                <FormControl label={item.label} labelTag="h3">
                  <Controller
                    as={<Radio.Group />}
                    name={item.name}
                    defaultValue={state.data[item.name] ?? item.defaultValue}
                    onChange={([e]) => {
                      return e.target.value;
                    }}
                    control={control}
                    options={item.options}
                  />
                </FormControl>
              </Col>
            ))}
          </Row>
        </FormContent>
        <FormAction>
          <div className="inner-wrapper">
            <Button
              className="back-btn"
              htmlType="button"
              onClick={() => setStep(6)}
            >
              <IoIosArrowBack /> {t('backBtn')}
            </Button>
            <Button type="primary" htmlType="submit">
              {t('submitBtn')}
            </Button>
          </div>
        </FormAction>
      </form>
    </Spin>
  );
};

export default AdditionalInformation;
