import React, { useState, useEffect } from 'react';
import { useStateMachine } from 'little-state-machine';
import { useForm, Controller } from 'react-hook-form';
import { Row, Col, Input, InputNumber, Button, Select, message } from 'antd';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import FormControl from 'components/UI/FormControl/FormControl';
import AddListingAction from './AddListingAction';
import { FormHeader, Title, FormContent, FormAction } from './AddListing.style';
import { LANGUAGES_LIST } from 'settings/languages-constants'
import { i18n } from 'i18n'
import CardButton from 'components/UI/CardButton/CardButton'
import { FaGlobe, FaDesktop, FaUsers } from 'react-icons/fa'
import { SaveOutlined } from '@ant-design/icons'

const PreBasicInformation = ({ t, setStep, event, user }) => {
  const { action, state } = useStateMachine(AddListingAction);
  const { handleSubmit } = useForm()
  const [eventType, setEventType] = useState(event.eventTypes)

  const isOnline = eventType === 'online' ? true : false

  const handleEventType = (value) => {
    setEventType(value)
  }

  const onSubmit = (data) => {
    setStep(2);
  };

  const saveHandler = async () => {
    const req = await fetch(`${process.env.API_URL}/events/${event.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + user.accessToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        eventTypes: eventType
      })
    })
    const res = await req.json()

    if (res.status === 'success') {
      message.success(`Тип мероприятия успешно изменен!`);
    }
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('whichEventType')}</Title>
          {/* <p>Online experiences can be hosted from anywhere through video, and in-person experiences are hosted on location</p> */}
        </FormHeader>
        <Row gutter={30} className="cards-event-type">
          <Col md={12} sm={24} align="right">
            <div onClick={() => handleEventType('online')}>
              <CardButton faIcon={<FaDesktop size={30} />} title={t('online')} content={t('onlineDesc')} className={isOnline ? 'selected' : ''} />
            </div>
          </Col>
          <Col md={12} sm={24}>
            <div onClick={() => handleEventType('offline')}>
              <CardButton faIcon={<FaUsers size={30} />} title={t('offline')} content={t('offlineDesc')} className={!isOnline ? 'selected' : ''} />
            </div>
          </Col>
        </Row>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
          <Button
            className="back-btn"
            type="default"
            htmlType="button"
            icon={<SaveOutlined />}
            onClick={saveHandler}
          >
            {t('save')}
          </Button>
          <Button type="primary" htmlType="submit">
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default PreBasicInformation;
