import React, { useEffect, useState } from 'react';
import { IoIosArrowBack } from 'react-icons/io';
import { useStateMachine } from 'little-state-machine';
import { useForm } from 'react-hook-form';
import { Button, message } from 'antd';
import DragAndDropUploader from 'components/UI/ImageUploader/DragAndDropUploader';
import DragAndDropCropUploader from 'components/UI/ImageUploader/DragAndDropCropUploader';
import FormControl from 'components/UI/FormControl/FormControl';
import AddListingAction from './AddListingAction';
import { SaveOutlined } from '@ant-design/icons'
import {
  FormHeader,
  Title,
  FormContent,
  FormAction,
  ImagesFormContainer,
  ImagesFieldWrapper,
  ImagesFieldPreviewVertical,
  ImagesFieldPreviewHorizontal
} from './AddListing.style';

const EventImages = ({ t, setStep, event, user }) => {
  const { register, errors, setValue, handleSubmit } = useForm({});
  const [loader, setLoader] = useState(false)
  const { action, state } = useStateMachine(AddListingAction);

  useEffect(() => {
    register({ name: 'coverPicture' }, { required: false });
    register({ name: 'previewPicture' }, { required: false });
  }, [register]);

  const onSubmit = async (data) => {
    const req = await fetch(`${process.env.API_URL}/events/${event.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': 'Bearer ' + user.accessToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        previewPicture: JSON.stringify(data.previewPicture),
        coverPicture: JSON.stringify(data.coverPicture)
      })
    })
    const res = await req.json()

    if (res.status === 'success') {
      message.success(`Изображение успешно обновлено!`);
    }
  };

  const onUploadPreview = (data) => {
    setValue('previewPicture', null)
    setValue('previewPicture', data)
  }

  const nextHandler = async () => {
    setStep(5);
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormContent>
        <FormHeader>
          <Title>{t('step4Header')}</Title>
        </FormHeader>
        <h2>{t('coverPicture')}</h2>
        <p>{t('coverSize')}</p>
        <ImagesFormContainer>
          <FormControl
            error={errors.coverPicture && <span>{t('requiredField')}</span>}
          >
            <ImagesFieldPreviewHorizontal>
              <DragAndDropUploader
                name="coverPicture"
                value={state.data.coverPicture}
                onUploadChange={(data) => setValue('coverPicture', data)}
                imageExist={state.data.coverPicture}
              />
            </ImagesFieldPreviewHorizontal>
          </FormControl>
          <h2>{t('previewPicture')}</h2>
          <p>{t('previewSize')}</p>
          <FormControl
            error={errors.previewPicture && message.error(t('previewPictureError'))}
          >
            <ImagesFieldPreviewVertical>
              <DragAndDropCropUploader
                name="previewPicture"
                value={state.data.previewPicture}
                onUploadChange={(data) => onUploadPreview(data)}
                imageExist={state.data.previewPicture}
              />
            </ImagesFieldPreviewVertical>
          </FormControl>
        </ImagesFormContainer>
      </FormContent>
      <FormAction>
        <div className="inner-wrapper">
        <Button
            className="back-btn"
            type="default"
            loading={loader}
            htmlType="submit"
            icon={<SaveOutlined />}
          >
            {t('save')}
          </Button>
          <Button type="primary" htmlType="button" onClick={nextHandler}>
            {t('nextBtn')}
          </Button>
        </div>
      </FormAction>
    </form>
  );
};

export default EventImages;
