import styled from 'styled-components';

const SecureHtmlTextWrapper = styled.div`
  // padding: 0 0 29px;
  word-break: break-word;
  white-space: pre-wrap;

  ul, ol {
    padding: 0 20px;
  }

  ul li {
    list-style-type: disc;
  }
  
  ol li {
    list-style-type: decimal;
  }
`;

export default SecureHtmlTextWrapper
