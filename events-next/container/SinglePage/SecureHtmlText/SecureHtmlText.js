import React from 'react';
import PropTypes from 'prop-types';
import { Element } from 'react-scroll';
import SecureHtmlTextWrapper from './SecureHtmlText.style'

const SecureHtmlText = ({ content }) => {
    return (
        <Element name="overview" className="overview">
            <SecureHtmlTextWrapper dangerouslySetInnerHTML={{ __html: content }} />
        </Element>
    );
};

SecureHtmlText.propTypes = {
    content: PropTypes.string
}

export default SecureHtmlText
