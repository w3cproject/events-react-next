import React from 'react';
import PropTypes from 'prop-types';
import { Element } from 'react-scroll';
import Rating from 'components/UI/Rating/Rating';
import Heading from 'components/UI/Heading/Heading';
import Text from 'components/UI/Text/Text';
import { Button, Tag } from 'antd';
import { DescriptionWrapper, CategoriesWrapper } from './Description.style';
import { RatingMeta, TextButton } from '../SinglePageView.style';
import SecureHtmlText from 'container/SinglePage/SecureHtmlText/SecureHtmlText'
import { i18n, Link } from 'i18n'

const Description = ({
  title,
  location,
  content,
  rating,
  ratingCount,
  categories,
  language,
  titleStyle,
  locationMetaStyle,
  contentStyle,
  linkStyle,
}) => {
  return (
    <Element name="overview" className="overview">
      <DescriptionWrapper>
        <Heading as="h2" content={title} {...titleStyle} />
        <CategoriesWrapper>
          {categories.map((c, index) => (
            <Tag key={index}>
              <Link href={`/events?categories=${c.slug}`}>
                <a target="_blank">
                  {i18n.language === 'ru'
                    ? c.title
                    : c.title_en}
                </a>
              </Link>
            </Tag>
          ))}
        </CategoriesWrapper>
        {location && <Text content={location.formattedAddress} {...locationMetaStyle} />}
        <RatingMeta>
          <Rating rating={rating} ratingCount={ratingCount} type="bulk" />
        </RatingMeta>
        <SecureHtmlText content={content} />
        {/* <Text content={content} {...contentStyle} /> */}
        {/* <TextButton>
          <Button>Read more about the hotel</Button>
        </TextButton> */}
      </DescriptionWrapper>
    </Element>
  );
};

Description.propTypes = {
  titleStyle: PropTypes.object,
  locationMetaStyle: PropTypes.object,
  contentStyle: PropTypes.object,
};

Description.defaultProps = {
  titleStyle: {
    color: '#2C2C2C',
    fontSize: ['17px', '20px', '25px'],
    lineHeight: ['1.15', '1.2', '1.36'],
    mb: '4px',
  },
  locationMetaStyle: {
    fontSize: '13px',
    fontWeight: '400',
    color: '#909090',
  },
  contentStyle: {
    fontSize: '15px',
    fontWeight: '400',
    color: '#2C2C2C',
    lineHeight: '1.6',
  },
};

export default Description;
