import styled from 'styled-components';

export const BookingWrapper = styled.div`
  padding: 0 30px;
`;

export const PreviewImage = styled.img`
  border-radius: 8px;
  height: 340px;
  width: auto;
  max-width: 270px;
`;

export const PaymentIcon = styled.img`
  height: 12px;
  width: auto;
  margin-left: 5px;
`;

export const BuySectionWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;