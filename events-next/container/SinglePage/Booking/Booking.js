import { useState } from 'react'
import PropTypes from 'prop-types';
import { Avatar, Button, Typography, Row, Col, Divider, Input, Select } from 'antd';
import Container from 'components/UI/Container/Container'
import { useForm, Controller } from 'react-hook-form'
import { BookingWrapper, PreviewImage, BuySectionWrapper, PaymentIcon } from './Booking.style'
import FormControl from 'components/UI/FormControl/FormControl'
import NumberFormat from 'react-number-format'
import { Link } from 'i18n'
import { ArrowLeftOutlined, LockOutlined, ClockCircleOutlined } from '@ant-design/icons'
import InputMasked, { inputPhoneMask } from 'components/UI/InputMasked/InputMasked'
// import MaskedInput from 'react-text-mask';
import moment from 'moment'

import visaIcon from 'assets/images/payment-icons/visa.svg'
import gPayIcon from 'assets/images/payment-icons/gpay.svg'
import { range } from 'lodash';

const { Title, Text } = Typography
const { Option } = Select

const Booking = ({ t, event, authUser, userProfile }) => {
    const [totalAmount, setTotalAmount] = useState(event.price)
    const [ticketsCount, setTicketsCount] = useState(1)
    const [loader, setLoader] = useState(false)
    const { control, errors, setError, setValue, getValues, handleSubmit } = useForm({})

    const onSubmit = async (data) => {
        if (!userProfile.phone && data.phone.length < 17) {
            setError('phone')
            return false
        }

        setLoader(true)
        const formBody = {
            ticketsCount: ticketsCount
        }

        formBody.email = userProfile.email ?? data.email;
        formBody.firstName = userProfile.firstName ?? data.firstName;
        formBody.lastName = userProfile.lastName ?? data.lastName;
        formBody.phone = userProfile.phone ?? data.phone;

        const req = await fetch(`${process.env.API_URL}/events/${event.id}/buy`, {
            method: 'POST',
            headers: {
                // 'Authorization': 'Bearer ' + user.accessToken,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formBody)
        })
        const res = await req.json()

        setTimeout(() => {
            if (res.status === 'success') {
                window.location = res.paymentUrl
            } else {
                // message.error(Object.keys(res.message)[0] + ': ' + Object.values(res.message)[0], 5)
            }

            setLoader(false)
        }, 2000)
    }

    const onTicketsCountHandler = value => {
        setTotalAmount(value * event.price)
        setTicketsCount(value)
    }

    return (
        <Container>
            <Row gutter={30} style={{ marginTop: 30 }}>
                <Col span={24} style={{ marginBottom: '30px' }}>
                    <Link href={`/events/${event.id}`}>
                        <a>
                            <Text>
                                <ArrowLeftOutlined style={{ marginRight: '5px' }} />
                                Назад к мероприятию
                            </Text>
                        </a>
                    </Link>
                </Col>
                <Col xs={{ order: 2 }} md={24} lg={{ span: 17, order: 1 }}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        {<Row gutter={30}>
                            {(!userProfile || !userProfile.firstName) && <Col xs={24} md={12}>
                                <FormControl
                                    label={t('firstName')}
                                    htmlFor="firstName"
                                    error={errors.firstName && <span>{t('requiredField')}</span>}
                                >
                                    <Controller
                                        as={<Input />}
                                        id="firstName"
                                        name="firstName"
                                        control={control}
                                        rules={{ required: true }}
                                    />
                                </FormControl>
                            </Col>}
                            {(!userProfile || !userProfile.lastName) && <Col xs={24} md={12}>
                                <FormControl
                                    label={t('lastName')}
                                    htmlFor="lastName"
                                    error={errors.lastName && <span>{t('requiredField')}</span>}
                                >
                                    <Controller
                                        as={<Input />}
                                        id="lastName"
                                        name="lastName"
                                        control={control}
                                        rules={{ required: true }}
                                    />
                                </FormControl>
                            </Col>}
                            {(!userProfile || !userProfile.email) && <Col xs={24} md={12}>
                                <FormControl
                                    label={t('email')}
                                    htmlFor="email"
                                    error={
                                        errors.email && (
                                            <>
                                                {errors.email?.type === 'required' && (
                                                    <span>{t('requiredField')}</span>
                                                )}
                                                {errors.email?.type === 'pattern' && (
                                                    <span>{t('validEmailField')}</span>
                                                )}
                                            </>
                                        )
                                    }
                                >
                                    <Controller
                                        as={<Input />}
                                        type="email"
                                        id="email"
                                        name="email"
                                        control={control}
                                        rules={{
                                            required: false,
                                            // pattern: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
                                        }}
                                    />
                                </FormControl>
                            </Col>}
                            {(!userProfile || !userProfile.phone) && <Col xs={24} md={12}>
                                <FormControl
                                    label={t('phone')}
                                    htmlFor="phone"
                                    error={errors.phone && <span>{t('requiredField')}</span>}
                                >
                                    <Controller
                                        as={<InputMasked />}
                                        type="phone"
                                        id="phone"
                                        value={getValues(['phone'])}
                                        mask={inputPhoneMask}
                                        name="phone"
                                        onChange={([e]) => {
                                            setValue('phone', e)
                                            return e
                                        }}
                                        control={control}
                                        rules={{ required: true }}
                                    />
                                </FormControl>
                            </Col>}
                        </Row>}
                        <Row gutter={30}>
                            <Col xs={24} md={12}>
                                <FormControl
                                    label={t('ticketsCount')}
                                >
                                    <Select onChange={onTicketsCountHandler} defaultValue={1}>
                                        {range(1, 11).map((value, index) => (
                                            <Option key={index} value={value}>{value}</Option>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Col>
                        </Row>
                        <BuySectionWrapper>
                            <Button loading={loader} htmlType="submit" type="primary" size="large" icon={<LockOutlined />}>
                                Купить
                            </Button>
                            <div>
                                <PaymentIcon src={visaIcon} />
                                <PaymentIcon src={gPayIcon} />
                            </div>
                        </BuySectionWrapper>
                        <Title level={5}>
                            Всего билетов: {ticketsCount}
                        </Title>
                        <Title level={5}>
                            <NumberFormat value={totalAmount}
                                displayType={'text'}
                                prefix={'На сумму: '}
                                suffix={` ${event.currency === 'rub' ? '₽' : '$'}`}
                                thousandSeparator={true} />
                        </Title>
                    </form>
                </Col>
                <Col xs={{ order: 1 }} md={24} lg={{ span: 7, order: 2 }}>
                    <PreviewImage style={{ textAlign: 'center' }} src={`${process.env.AMAZON_ASSETS_URL}/${event.previewPicture}?width=270&height=340`} />
                    <Title level={4} style={{ marginTop: '5px' }}>
                        {event.title}
                    </Title>
                    <Divider />
                    <Title level={5}>
                        <ClockCircleOutlined style={{ marginRight: '5px' }} />
                        {moment(event.startDate).format('DD.MM.Y, HH:mm')}
                    </Title>
                    <Divider />
                    <Link href={`/u/${event.owner.id}`}>
                        <a style={{ marginTop: '10px', color: '#008489', fontWeight: '700', fontSize: '15px' }} target="_blank">
                            {event.owner.profilePicture
                                ? <Avatar className="mr3"
                                    src={process.env.AMAZON_ASSETS_URL + '/' + event.owner.profilePicture}
                                    size={26} />
                                : <Avatar className="mr3"
                                    style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}
                                    size={26}>U</Avatar>}
                            <span style={{ marginLeft: '10px' }}>
                                {event.owner.firstName
                                    ? event.owner.firstName + ' ' + event.owner.lastName
                                    : 'Организатор'}
                            </span>
                        </a>
                    </Link>
                </Col>
            </Row>
        </Container>
    )
}

Booking.propTypes = {
    event: PropTypes.object.isRequired,
    authUser: PropTypes.object,
    userProfile: PropTypes.object
};

export default Booking