import React from 'react';
import PropTypes from 'prop-types'
import ImageGallery from 'react-image-gallery';
import ImageGalleryWrapper from './ImageGallery.style';

const PostImageGallery = ({ images }) => {
  return (
    <ImageGalleryWrapper>
      <ImageGallery
        items={images}
        showPlayButton={false}
        showFullscreenButton={false}
        showIndex={true}
        lazyLoad={true}
        slideDuration={550}
      />
    </ImageGalleryWrapper>
  );
};

PostImageGallery.propTypes = {
  images: PropTypes.array,
}

export default PostImageGallery
