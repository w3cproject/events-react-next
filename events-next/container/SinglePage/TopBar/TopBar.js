import React from 'react';
import Sticky from 'react-stickynode';
import {
  FaceBookShare,
  TwitterShare,
  LinkedInShare,
  PinterestShare,
  VkShare,
  TelegramShare,
  EmailShare,
  ShortLinkShare
} from 'components/SocialShare/SocialShare';
import { Button, Menu, Dropdown } from 'antd';
import Favorite from 'components/UI/Favorite/Favorite';
import ScrollBar from 'components/UI/ScrollBar/ScrollBar';
import { Link } from 'i18n'
import { TobBarWrapper, ButtonGroup } from '../SinglePageView.style';
import { ROLE_SUPER_ADMIN } from 'settings/constant.js'

const topBarMenu = [
  {
    name: 'Описание',
    target: 'overview',
  },
  /*   {
      name: 'Amenities',
      target: 'amenities',
    }, */
  /* {
    name: 'Location',
    target: 'location',
  },
  {
    name: 'Reviews',
    target: 'reviews',
  }, */
];

const SocialShareMenu = (props) => {
  return (
    <Menu>
      <Menu.Item>
        <ShortLinkShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <TwitterShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <FaceBookShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <LinkedInShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <PinterestShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <TelegramShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <VkShare {...props} />
      </Menu.Item>
      <Menu.Item>
        <EmailShare {...props} />
      </Menu.Item>
    </Menu>
  );
};

const SideButtons = (props) => {
  const { event, user, inFavorites } = props
  const handleFavorites = async () => {
    const res = await fetch(`${process.env.API_URL}/users/favorites`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.accessToken}`
      },
      body: JSON.stringify({ eventId: event.id })
    })
    const data = await res.json()
  }

  let haveEditAccess = false

  if (user) {
    haveEditAccess = event.owner.id === user.id || user.roles.includes(ROLE_SUPER_ADMIN)
  }

  return (
    <ButtonGroup>
      {haveEditAccess &&
        <>
          <Button>
            <Link href={`/events/${event.id}/manage`}>
              <a target="_blank">
                Управление мероприятием
        </a>
            </Link>
          </Button>
          <Button>
            <Link href={`/events/${event.id}/update`}>
              <a target="_blank">
                Редактировать
        </a>
            </Link>
          </Button>
        </>}
      <Favorite className="ant-btn" onClick={handleFavorites} defaultValue={inFavorites} />
      <Dropdown
        placement="bottomRight"
        overlay={() => <SocialShareMenu {...props} />}
        overlayClassName="social_share_menu"
      >
        <Button className="ant-dropdown-link">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.309 15.537">
            <path
              d="M80.68,101.873,74.507,96.1a.316.316,0,0,0-.245-.105c-.193.009-.438.144-.438.35v2.9a.187.187,0,0,1-.158.179c-6.138.941-8.724,5.535-9.639,10.3-.035.188.219.363.337.214a11.158,11.158,0,0,1,9.275-4.7.216.216,0,0,1,.184.21v2.844a.375.375,0,0,0,.634.232l6.217-5.876a.483.483,0,0,0,.153-.368A.586.586,0,0,0,80.68,101.873Z"
              transform="translate(-63.271 -95.242)"
            />
          </svg>
        </Button>
      </Dropdown>
    </ButtonGroup>
  );
};

const TopBar = (props) => {
  const { t, title, shareURL, author, media, event, user, inFavorites } = props;
  return (
    <TobBarWrapper>
      <Sticky innerZ={10} top={80} activeClass="isSticky">
        <ScrollBar
          menu={topBarMenu}
          t={t}
          other={
            <SideButtons
              media={media}
              author={author}
              title={title}
              shareURL={shareURL}
              event={event}
              user={user}
              inFavorites={inFavorites}
            />
          }
        />
      </Sticky>
    </TobBarWrapper>
  );
};

export default TopBar;
