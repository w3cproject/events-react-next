import React, { useState, Fragment } from 'react';
import { IoIosClose } from 'react-icons/io';
import Rating from 'components/UI/Rating/Rating';
import { Button, Modal } from 'antd';
import StickyBooking from 'components/StickyBooking/StickyBooking';
import LogoImage from 'assets/images/logo-alt.svg';
import Reservation from './Reservation';
import { Link } from 'i18n'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { LOGIN_PAGE } from 'settings/constant'

const ticketsCloudToken = process.env.TICKETS_CLOUD_TOKEN

const BottomReservation = ({ t, event, user, title, price, rating, ratingCount }) => {
  const [state, setState] = useState(false);
  const toggleModal = () => {
    setState(!state);
  };
  const handleCancel = () => {
    setState(!state);
  };

  return (
    <Fragment>
      <StickyBooking
        logo={LogoImage}
        title={title}
        price={price}
        rating={
          <Rating rating={rating} ratingCount={ratingCount} type="bulk" />
        }
        action={
          <>
            {!user && event.ticketsCloudId &&
              <Link href={LOGIN_PAGE}>
                <Button type="primary" icon={<ShoppingCartOutlined />}>
                  {t('login')}
                </Button>
              </Link>}

            {user && event.ticketsCloudId &&
              <Button type="primary" icon={<ShoppingCartOutlined />}
                data-tc-event={event.ticketsCloudId}
                data-tc-token={ticketsCloudToken}>
                {t('buyBtn')}</Button>}

            {!event.ticketsCloudId && event.price > 0 &&
              <Link href={`/events/${event.id}/book`}>
                <Button type="primary" icon={<ShoppingCartOutlined />}>
                  {t('buyBtn')}
                </Button>
              </Link>}

            {event.price === 0 &&
              <Button type="primary" icon={<ShoppingCartOutlined />}>
                Регистрация
            </Button>}
          </>
        }
      />

      <Modal
        visible={state}
        onCancel={handleCancel}
        footer={null}
        maskStyle={{
          backgroundColor: 'rgba(255, 255, 255, 0.8)',
        }}
        wrapClassName="reservation_modal"
        closable={false}
      >
        <Reservation />
        <Button onClick={handleCancel} className="close">
          <IoIosClose />
        </Button>
      </Modal>
    </Fragment>
  );
};

export default BottomReservation;
