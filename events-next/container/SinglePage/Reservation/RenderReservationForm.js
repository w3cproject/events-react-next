import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'i18n'
import { Button } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons';
import ReservationFormWrapper, { FormActionArea } from './Reservation.style.js';
import { LOGIN_PAGE } from 'settings/constant.js'
import moment from 'moment'

const ticketsCloudToken = process.env.TICKETS_CLOUD_TOKEN

const EndedButton = ({ t }) => {
  return (
    <Button htmlType="submit" type="primary">
      {t('isEnded')}
    </Button>
  )
}

const ActiveButton = ({ t, event, user }) => {
  return (
    <>
      {!user && event.ticketsCloudId &&
        <Link href={LOGIN_PAGE}>
          <Button htmlType="submit" type="primary" icon={<ShoppingCartOutlined />}>
            {t('login')}
          </Button>
        </Link>}

      {user && event.ticketsCloudId &&
        <Button htmlType="submit" type="primary" icon={<ShoppingCartOutlined />}
          data-tc-event={event.ticketsCloudId}
          data-tc-token={ticketsCloudToken}>
          {t('buyBtn')}</Button>}

      {!event.ticketsCloudId && event.price > 0 &&
        <Link href={`/events/${event.id}/book`}>
          <Button htmlType="submit" type="primary" icon={<ShoppingCartOutlined />}>
            {t('buyBtn')}
          </Button>
        </Link>}

      {event.price === 0 &&
        <Button htmlType="submit" type="primary" icon={<ShoppingCartOutlined />}>
          Регистрация
        </Button>}
    </>
  )
}

const RenderReservationForm = ({ t, event, user }) => {
  const startDate = moment(event.startDate)
  const currentDate = moment(new Date())
  const eventIsEnded = startDate.isBefore(currentDate, 'minute')

  return (
    <ReservationFormWrapper className="form-container">
      <FormActionArea>
        {eventIsEnded
          ? <EndedButton t={t} />
          : <ActiveButton t={t} user={user} event={event} />}
      </FormActionArea>
    </ReservationFormWrapper >
  );
};

RenderReservationForm.propTypes = {
  price: PropTypes.number,
  currency: PropTypes.string,
};

export default RenderReservationForm;
