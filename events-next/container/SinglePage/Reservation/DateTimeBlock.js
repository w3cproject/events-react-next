import moment from 'moment'
import PropTypes from 'prop-types';
import { Typography } from 'antd'
import { ClockCircleOutlined } from '@ant-design/icons'

const { Title } = Typography

const defaultFormat = 'DD.MM.Y'
const defaultLevel = 5

const DateTimeBlock = ({ date, time, dateFormat, level }) => {
    return (
        <Title level={level}>
            <ClockCircleOutlined style={{ marginRight: '5px' }} />
            {moment(date).format(dateFormat)}, {time}
        </Title>
    )
}

DateTimeBlock.propTypes = {
    date: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    dateFormat: PropTypes.string,
    level: PropTypes.number,
}

DateTimeBlock.defaultProps = {
    dateFormat: defaultFormat,
    level: defaultLevel
}

export default DateTimeBlock