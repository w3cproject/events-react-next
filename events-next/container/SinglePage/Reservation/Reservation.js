import React, { Fragment } from 'react';
import { Link } from 'i18n';
import PropTypes from 'prop-types';
import Card from 'components/UI/Card/Card';
import Heading from 'components/UI/Heading/Heading';
import RenderReservationForm from './RenderReservationForm';
import DateTimeBlock from './DateTimeBlock'
import NumberFormat from 'react-number-format'
import { Avatar } from 'antd'

const CardHeader = ({ t, user, priceStyle, pricePeriodStyle, linkStyle, price, currency, owner }) => {
  return (
    <Fragment>
      <Heading
        content={
          <Fragment>
            {price ? <NumberFormat value={price}
              displayType={'text'}
              prefix={t('from')}
              suffix={`${currency === 'rub' ? '₽' : '$'} / ${t('person')}`}
              thousandSeparator={true} />
              : <span>Бесплатно</span>}
            {/* ${price} <Text as="span" content="/ person" {...pricePeriodStyle} /> */}
          </Fragment>
        }
        {...priceStyle}
      />
      <Link href={`/u/${owner.id}`}>
        <a style={{ ...linkStyle, marginTop: '10px' }} target="_blank">
          {owner.profilePicture
            ? <Avatar className="mr3"
              src={process.env.AMAZON_ASSETS_URL + '/' + owner.profilePicture}
              size={26} />
            : <Avatar className="mr3"
              style={{ color: '#f56a00', backgroundColor: '#fde3cf' }}
              size={26}>U</Avatar>}
          <span style={{ marginLeft: '10px' }}>
            {owner.firstName
              ? owner.firstName + ' ' + owner.lastName
              : 'Организатор'}
          </span>
        </a>
      </Link>
    </Fragment>
  );
};

export default function Reservation({ t, linkStyle, event, price, currency, eventId, user, owner }) {
  return (
    <Card
      className="reservation_sidebar"
      header={<CardHeader t={t} user={user} price={price} currency={currency} owner={owner} />}
      content={<RenderReservationForm t={t} event={event} user={user} />}
      footer={
        <>
          <DateTimeBlock date={event.startDate} time={event.startTime} />
          {/* <span>{event.duration}</span> */}
          {/* <p>Уже:</p>
        <Avatar.Group maxCount={2} maxStyle={{ color: '#f56a00', backgroundColor: '#fde3cf' }}>
          <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          <Avatar style={{ backgroundColor: '#f56a00' }}>K</Avatar>
          <Tooltip title="Ant User" placement="top">
            <Avatar style={{ backgroundColor: '#87d068' }} icon={<UserOutlined />} />
          </Tooltip>
          <Avatar style={{ backgroundColor: '#1890ff' }} icon={<AntDesignOutlined />} />
        </Avatar.Group> */}
        </>
      }
    />
  );
}

CardHeader.propTypes = {
  priceStyle: PropTypes.object,
  pricePeriodStyle: PropTypes.object,
  linkStyle: PropTypes.object,
  price: PropTypes.number,
  currency: PropTypes.string,
};

Reservation.propTypes = {
  price: PropTypes.number,
  currency: PropTypes.string,
  eventId: PropTypes.string,
}

CardHeader.defaultProps = {
  priceStyle: {
    color: '#2C2C2C',
    fontSize: '20px',
    fontWeight: '700',
  },
  pricePeriodStyle: {
    fontSize: '15px',
    fontWeight: '400',
  },
  linkStyle: {
    fontSize: '15px',
    fontWeight: '700',
    color: '#008489',
  },
};
