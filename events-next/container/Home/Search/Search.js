import React from 'react';
import PropTypes from 'prop-types';
import Container from 'components/UI/Container/Container';
import Heading from 'components/UI/Heading/Heading';
import Text from 'components/UI/Text/Text';
import GlideCarousel, {
  GlideSlide,
} from 'components/UI/GlideCarousel/GlideCarousel';
import SearchForm from './SearchForm';
import { withTranslation } from 'i18n'
import BannerWrapper, { SearchWrapper } from './Search.style';
// slider images
import bannerBg17 from 'assets/images/banner/17.jpg';

const HomeSearch = ({ t, categories, searchTitleStyle, searchDescriptionStyle }) => {
  return (
    <BannerWrapper>
      <GlideCarousel
        controls={false}
        options={{ gap: 0, autoplay: 5000, animationDuration: 1000 }}
        bullets={false}
        numberOfBullets={1}
      >
        <>
          <GlideSlide>
            <img src={bannerBg17} alt="Banner" />
          </GlideSlide>
        </>
      </GlideCarousel>

      <Container>
        <SearchWrapper>
          <Heading
            {...searchTitleStyle}
            content="Новые события - каждый день!"
          />
          <Text
            {...searchDescriptionStyle}
            content="Более 6,000 мероприятий по всему миру."
          />
          <SearchForm t={t} categories={categories} />
        </SearchWrapper>
      </Container>
    </BannerWrapper>
  );
};

HomeSearch.propTypes = {
  searchTitleStyle: PropTypes.object,
  searchDescriptionStyle: PropTypes.object,
};

HomeSearch.defaultProps = {
  searchTitleStyle: {
    color: '#2C2C2C',
    fontSize: ['20px', '24px', '28px'],
    lineHeight: ['28px', '30px', '30px'],
    mb: '9px',
  },
  searchDescriptionStyle: {
    color: '#2C2C2C',
    fontSize: '15px',
    lineHeight: '18px',
    mb: '30px',
  },
};

HomeSearch.defaultProps = {
  i18nNamespaces: ['searchForm']
}

HomeSearch.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('searchForm')(HomeSearch)
