import React, { useState, useContext } from 'react';
import { withRouter } from 'next/router';
import { i18n, Router } from 'i18n';
import isEmpty from 'lodash/isEmpty';
import { FaMapMarkerAlt, FaRegCalendar, FaRegListAlt, FaMoneyBill } from 'react-icons/fa';
import { Button, Checkbox, Select, Slider } from 'antd';
import MapAutoComplete from 'components/Map/MapAutoComplete';
import InputIncDec from 'components/UI/InputIncDec/InputIncDec';
import DateRangePickerBox from 'components/UI/DatePicker/ReactDates';
import { mapDataHelper } from 'components/Map/mapDataHelper';
import ViewWithPopup from 'components/UI/ViewWithPopup/ViewWithPopup';
import NumberFormat from 'react-number-format'
import { langToSign } from 'library/helpers/currency_helper.js'
import {
  FormWrapper,
  ComponentWrapper,
  RoomGuestWrapper,
  ItemWrapper,
} from './Search.style';
import { EVENTS_LIST_PAGE } from '../../../settings/constant';
import { SearchContext } from '../../../context/SearchProvider';

const { Option } = Select;

const calendarItem = {
  separator: '-',
  format: 'MM-DD-YYYY',
  locale: 'en',
};

const timesOptions = {
  options: [
    { label: 'Утро до 12:00', value: 'free-wifi' },
    { label: 'Утро после 12:00', value: 'free-parking' },
    { label: 'Вечер после 17:00', value: 'breakfast' }
  ],
};

const minPrice = 100
const maxPrice = 8000

export const priceInit = {
  100: minPrice,
  8000: maxPrice,
};

const SearchForm = ({ t, categories }) => {
  const { state, dispatch } = useContext(SearchContext);
  const [searchDate, setSearchDate] = useState({
    setStartDate: null,
    setEndDate: null,
  });
  const [mapValue, setMapValue] = useState([]);
  const [roomGuest, setRoomGuest] = useState({
    room: 0,
    guest: 0,
  });
  const [categoriesSelected, setCategoriesSelected] = useState([])
  const [price, setPrice] = useState([100, 8000])

  const categoriesOptions = [];

  categories.map((category) => {
    categoriesOptions.push({
      label: (i18n.language === 'ru') ? category.title : category.title_en,
      value: category.slug
    })
  })

  const onChangeCategory = (value) => {
    const oldCategories = categoriesSelected

    if (oldCategories.includes(value)) {
      const newCategories = oldCategories.filter((slug) => slug !== value)
      setCategoriesSelected(newCategories)
    } else {
      setCategoriesSelected([value])
    }
  }

  const onChangePrice = (value) => {
    setPrice([value[0], value[1]])
  }

  const updatevalueFunc = (event) => {
    const { searchedPlaceAPIData } = event;
    if (!isEmpty(searchedPlaceAPIData)) {
      setMapValue(searchedPlaceAPIData);
    }
  };

  const handleIncrement = (type) => {
    setRoomGuest({
      ...roomGuest,
      [type]: roomGuest[type] + 1,
    });
  };

  const handleDecrement = (type) => {
    if (roomGuest[type] <= 0) {
      return false;
    }
    setRoomGuest({
      ...roomGuest,
      [type]: roomGuest[type] - 1,
    });
  };

  const handleIncDecOnChnage = (e, type) => {
    let currentValue = e.target.value;
    setRoomGuest({
      ...roomGuest,
      [type]: currentValue,
    });
  };

  const goToSearchPage = () => {
    let tempLocation = [],
      dateRange = {},
      location_lat = '',
      location_lng = '';
    const mapData = mapValue ? mapDataHelper(mapValue) : [];
    mapData &&
      mapData.map((singleMapData, i) => {
        return tempLocation.push({
          formattedAddress: singleMapData ? singleMapData.formattedAddress : '',
          lat: singleMapData ? singleMapData.lat.toFixed(3) : null,
          lng: singleMapData ? singleMapData.lng.toFixed(3) : null,
        });
      });
    const location = tempLocation ? tempLocation[0] : {};
    dateRange.startDate = searchDate ? searchDate.setStartDate : null;
    dateRange.endDate = searchDate ? searchDate.setEndDate : null;

    if (location && location.lat) {
      location_lat = location.lat;
    }

    if (location && location.lng) {
      location_lng = location.lng;
    }

    let query = {
      setStartDate: searchDate.setStartDate,
      setEndDate: searchDate.setEndDate,
      categories: categoriesSelected[0],
      minPrice: price ? price[0] : minPrice,
      maxPrice: price ? price[1] : maxPrice,
      location_lat,
      location_lng,
    };

    for (const key in query) {
      if (!query[key]) {
        delete query[key];
      }
    }

    dispatch({
      type: 'UPDATE',
      payload: {
        ...state,
        setStartDate: searchDate.setStartDate,
        setEndDate: searchDate.setEndDate,
        categories: query.categories,
        maxPrice: query.minPrice,
        minPrice: query.maxPrice,
        location_lat,
        location_lng,
      },
    });

    const searchParams = new URLSearchParams(query)

    Router.push(`${EVENTS_LIST_PAGE}/?${searchParams.toString()}`);
  };

  return (
    <FormWrapper>
      <ComponentWrapper>
        <FaRegListAlt className="map-marker" />
        <ViewWithPopup
          key={200}
          noView={true}
          className="room_guest"
          withMaxHeight={true}
          view={
            <Button type="default" style={{ justifyContent: 'flex-start' }}>
              {categoriesSelected.length > 0
                ? categoriesOptions.filter(item => {
                  return categoriesSelected[0].includes(item.value)
                }).map((item, index) => (
                  <span key={index} className="category-selected-item">{item.label}</span>
                ))
                : <span>{t('categories')}</span>}
            </Button>
          }
          popup={
            <RoomGuestWrapper>
              <Checkbox.Group
                options={categoriesOptions}
                defaultValue={categoriesSelected[0]}
                onChange={(value) => onChangeCategory(value)}
              />
            </RoomGuestWrapper>
          }
        />
        {/* <MapAutoComplete updatevalue={(value) => updatevalueFunc(value)} />
        <svg
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          x="0px"
          y="0px"
          width="30"
          height="30"
          viewBox="0 0 144.083 144"
          enableBackground="new 0 0 144.083 144"
          className="target"
        >
          <path d="M117.292,69h-13.587C102.28,53.851,90.19,41.761,75.042,40.337V26.5h-6v13.837C53.893,41.761,41.802,53.851,40.378,69  H26.792v6h13.587c1.425,15.148,13.515,27.238,28.663,28.663V117.5h6v-13.837C90.19,102.238,102.28,90.148,103.705,75h13.587V69z   M72.042,97.809c-14.23,0-25.809-11.578-25.809-25.809c0-14.231,11.578-25.809,25.809-25.809S97.85,57.769,97.85,72  C97.85,86.23,86.272,97.809,72.042,97.809z" />
          <path d="M72.042,52.541c-10.729,0-19.459,8.729-19.459,19.459s8.729,19.459,19.459,19.459S91.5,82.729,91.5,72  S82.771,52.541,72.042,52.541z M72.042,85.459c-7.421,0-13.459-6.037-13.459-13.459c0-7.421,6.038-13.459,13.459-13.459  S85.5,64.579,85.5,72C85.5,79.422,79.462,85.459,72.042,85.459z" />
        </svg> */}
      </ComponentWrapper>

      <ComponentWrapper>
        <FaRegCalendar className="calendar" />
        <DateRangePickerBox
          item={calendarItem}
          startDateId="startDateId-id-home"
          endDateId="endDateId-id-home"
          startDatePlaceholderText={t('startDate')}
          endDatePlaceholderText={t('endDate')}
          updateSearchData={(setDateValue) => setSearchDate(setDateValue)}
          showClearDates={true}
          small={true}
          numberOfMonths={1}
        />
      </ComponentWrapper>

      <ComponentWrapper>
        <FaMoneyBill className="user-friends" />
        <ViewWithPopup
          key={200}
          noView={true}
          className="room_guest"
          view={
            <Button type="default">
              {price
                ? <span>
                  <NumberFormat value={price[0]}
                    displayType={'text'}
                    suffix=" - "
                    thousandSeparator={true} />
                  <NumberFormat value={price[1]}
                    displayType={'text'}
                    suffix={` ${langToSign(i18n.language)}`}
                    thousandSeparator={true} />
                </span>
                : <>{t('price')}</>}
            </Button>
          }
          popup={
            <RoomGuestWrapper>
              <Slider
                range
                marks={priceInit}
                min={minPrice}
                max={maxPrice}
                step={minPrice}
                defaultValue={price ? [price[0], price[1]] : [minPrice, maxPrice]}
                onAfterChange={onChangePrice}
              />
              <Checkbox.Group
                options={timesOptions}
                // defaultValue={amenities}
                onChange={(value) => onChange(value, 'amenities')}
              />
            </RoomGuestWrapper>
          }
        />
      </ComponentWrapper>

      <Button
        type="primary"
        htmlType="submit"
        size="large"
        onClick={goToSearchPage}
      >
        {t('findBtn')}
      </Button>
    </FormWrapper>
  );
};

export default withRouter(SearchForm);
