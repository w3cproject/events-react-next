import fetch from 'isomorphic-unfetch';
import shuffle from 'lodash/shuffle';

const FetchAPIData = (url, method = 'GET', token = null) => {
  let fetchParams = {
    method: method
  }

  if (token !== null) {
    fetchParams.headers = {
      'Authorization': `Bearer ${token}`
    }
  }

  return fetch(url, fetchParams)
    .then((r) => r.json())
    .then((data) => {
      return data;
    });
};

export const ProcessAPIData = (apiData) => {
  let fetchData = {};
  let withCount = false
  if (apiData) {
    apiData.forEach((item, key) => {
      if (item.name === 'eventSingleData') {
        fetchData.data = item.data.event ?? {}
      } else if (item.name === 'listingEvents') {
        fetchData.data = item.data.events ? [...item.data.events] : [];
        fetchData.count = item.data.count
        withCount = true
      } else {
        fetchData.data = item.data.events ? [...item.data.events] : [];
      }

      if (item.name === 'userProfile') {
        fetchData.data = item.data.user ?? {}
      }

      fetchData.name = item.name ? item.name : '';
    });
  }
  let data = fetchData ? fetchData.data : [];
  if (withCount) {
    data = fetchData
  }
  return data;
};

export const SearchedData = (processedData) => {
  const randNumber = Math.floor(Math.random() * 50 + 1);
  const data = shuffle(processedData.slice(0, randNumber));
  return data;
};

export const SearchStateKeyCheck = (state) => {
  for (var key in state) {
    if (
      state[key] !== null &&
      state[key] != '' &&
      state[key] != [] &&
      state[key] != 0 &&
      state[key] != 100
    ) {
      return true;
    }
  }
  return false;
};

export const Paginator = (posts, processedData, limit) => {
  return [...posts, ...processedData.slice(posts.length, posts.length + limit)];
};

export const Paginator2 = (posts, processedData) => {
  return [...posts, ...processedData];
};

const GetAPIData = async (apiUrl, method = 'GET', token = null) => {
  const promises = apiUrl.map(async (repo) => {
    // const apiPath = `${process.env.SERVER_API}/static/data`; // read it from env variable
    const api = `${process.env.API_URL}/${repo.endpoint}`;
    const response = await FetchAPIData(api, method, token);
    return {
      name: repo.name,
      data: response,
    };
  });
  const receviedData = await Promise.all(promises);
  return receviedData;
};

export default GetAPIData;
