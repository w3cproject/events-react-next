import { withRouter } from 'next/router';
import { Router } from 'i18n'

const ActiveLink = ({ className, children, href }) => {
  const handleClick = e => {
    e.preventDefault();
    Router.push(href);
  };

  return (
    <a className={className} href={href} onClick={handleClick}>
      {children}
    </a>
  );
};

export default withRouter(ActiveLink);
