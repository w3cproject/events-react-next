export const langToCurrency = (lang) => {
    switch (lang) {
        case 'ru':
            return 'RUB'
        default:
            return 'USD'
    }
}

export const langToSign = (lang) => {
    switch (lang) {
        case 'ru':
            return '₽'
        default:
            return '$'
    }
}

export const currencyToSign = (currency) => {
    switch (currency) {
        case 'RUB':
            return '₽'
        default:
            return '$'
    }
}