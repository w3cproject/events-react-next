import { parseCookies, destroyCookie } from 'nookies'
import Cookies from 'js-cookie'

const parseContext = (ctx) => {
    const cookies = parseCookies(ctx)

    return cookies
}


export const getUserBrowser = () => {
    return Cookies.get('user')
}


export const getUserSSR = (ctx) => {
    const { user } = parseContext(ctx)

    if (!user) {
        return false
    }

    return JSON.parse(user)
}


export const getUserTokenSSR = (ctx) => {
    const { accessToken } = getUserSSR(ctx)

    return accessToken
}

export const destroyLoginSSR = (ctx) => {
    destroyCookie(ctx, 'user')
    destroyCookie(ctx, 'loggedIn')

    return true
}