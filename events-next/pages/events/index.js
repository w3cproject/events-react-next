import { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import Sticky from 'react-stickynode';
import Toolbar from 'components/UI/Toolbar/Toolbar';
import CategorySearch from 'container/Listing/Search/CategorySearch/CategorySearch';
import { Checkbox } from 'antd';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import ListingMap from 'container/Listing/ListingMap';
import { SearchContext } from 'context/SearchProvider';
import GetAPIData, {
  Paginator,
  Paginator2,
  SearchedData,
  SearchStateKeyCheck,
  ProcessAPIData,
} from 'library/helpers/get_api_data';
import { getDeviceType } from 'library/helpers/get_device_type';
import { EVENTS_LIST_PAGE } from 'settings/constant';
import {
  LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP,
  LISTING_PAGE_COLUMN_WIDTH_WITH_MAP,
  EVENTS_LIST_PAGE_LIMIT
} from 'settings/config';
import ListingWrapper, {
  PostsWrapper,
  ShowMapCheckbox,
} from 'container/Listing/Listing.style';

const FilterDrawer = dynamic(() =>
  import('container/Listing/Search/MobileSearchView')
);

export default function EventsListPage({ processedData, totalItems, deviceType }) {
  const { state, dispatch } = useContext(SearchContext);
  const statekey = SearchStateKeyCheck(state);
  const [posts, setPosts] = useState(
    processedData.slice(0, EVENTS_LIST_PAGE_LIMIT) || []
  );
  const [loading, setLoading] = useState(false);
  const [showMap, setShowMap] = useState(false);
  const [eventsOffset, setEventsOffset] = useState(EVENTS_LIST_PAGE_LIMIT)

  useEffect(() => {
    if (statekey === true) {
      const newData = SearchedData(processedData);
      setPosts(newData);
    } else {
      setPosts(processedData.slice(0, EVENTS_LIST_PAGE_LIMIT) || []);
    }
  }, [statekey]);

  const handleMapToggle = () => {
    setShowMap((showMap) => !showMap);
  };

  const handleLoadMore = async () => {
    setLoading(true);
    const res = await fetch(`${process.env.API_URL}/events?limit=${EVENTS_LIST_PAGE_LIMIT}&offset=${eventsOffset}`)
    const resData = await res.json()

    if (resData.count === 0) {
      return false
    }

    setTimeout(() => {
      // const data = Paginator(posts, processedData, EVENTS_LIST_PAGE_LIMIT);
      const data = Paginator2(posts, resData.events);
      setPosts(data);
      setEventsOffset(eventsOffset + EVENTS_LIST_PAGE_LIMIT)
      setLoading(false);
    }, 1000);
  };

  let columnWidth = LISTING_PAGE_COLUMN_WIDTH_WITHOUT_MAP;
  if (showMap) {
    columnWidth = LISTING_PAGE_COLUMN_WIDTH_WITH_MAP;
  }

  let columnCount = 'col-24';
  if (deviceType === 'desktop' && showMap === true) {
    columnCount = 'col-12';
  }

  return (
    <ListingWrapper>
      <Head>
        <title>Events | Events list</title>
      </Head>

      <Sticky top={82} innerZ={999} activeClass="isHeaderSticky">
        {/* <Toolbar
          left={
            deviceType === 'desktop' ? <CategorySearch /> : <FilterDrawer />
          }
        right={
          <ShowMapCheckbox>
            <Checkbox defaultChecked={false} onChange={handleMapToggle}>
              Show map
            </Checkbox>
          </ShowMapCheckbox>
        }
        /> */}
      </Sticky>

      <PostsWrapper className={columnCount}>
        <SectionGrid
          link={EVENTS_LIST_PAGE}
          columnWidth={columnWidth}
          deviceType={deviceType}
          data={posts}
          totalItem={totalItems}
          limit={EVENTS_LIST_PAGE_LIMIT}
          loading={loading}
          handleLoadMore={handleLoadMore}
          placeholder={<PostPlaceholder />}
        />
      </PostsWrapper>
      {showMap && <ListingMap loading={loading} mapData={posts} />}
    </ListingWrapper>
  );
}

export async function getServerSideProps({ req, res, query }) {
  const { search, setStartDate, setEndDate, minPrice, maxPrice, categories } = query
  let queryFields = {
    startDate: setStartDate,
    endDate: setEndDate,
    categories: categories,
    minPrice: minPrice ?? 100,
    maxPrice: maxPrice ?? 8000,
    offset: 0,
    limit: 12,
    search: search
  };

  for (const key in queryFields) {
    if (!queryFields[key]) {
      delete queryFields[key];
    }
  }

  const searchParams = new URLSearchParams(queryFields)

  const apiUrl = [
    {
      endpoint: `events?${searchParams.toString()}`,
      name: 'listingEvents',
    },
  ];
  const pageData = await GetAPIData(apiUrl);
  const processedData = ProcessAPIData(pageData);
  const deviceType = getDeviceType(req);
  return {
    props: { processedData: processedData.data, totalItems: processedData.count, deviceType },
  };
}
