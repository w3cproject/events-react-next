import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types'
import UpdateListing from 'container/UpdateListing/UpdateListing';
import { withTranslation } from 'i18n'
import { getUserSSR } from 'library/helpers/get_user_data'
import { ROLE_SUPER_ADMIN } from 'settings/constant.js'

const UpdateEventPage = ({ t, user, categoriesData, event }) => {
  return (
    <>
      <Head>
        <title>Events | Update Event</title>
      </Head>
      <UpdateListing t={t} user={user} categories={categoriesData.categories} event={event} />
    </>
  );
}

UpdateEventPage.defaultProps = {
  i18nNamespaces: ['createEvent']
}

UpdateEventPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export async function getServerSideProps(context) {
  const { query } = context
  const user = getUserSSR(context)
  const eventId = query.id

  if (!user) {
    context.res.writeHead(302, { Location: '/auth/sign-in' })
    context.res.end()
  }

  const resEvent = await fetch(`${process.env.API_URL}/events/${eventId}`)
  const dataEvent = await resEvent.json()

  if (dataEvent.event.owner.id !== user.id && !user.roles.includes(ROLE_SUPER_ADMIN)) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }
  
  const res = await fetch(`${process.env.API_URL}/categories`)
  const data = await res.json()

  return {
    props: { user, categoriesData: data, event: dataEvent.event },
  };
}

export default withTranslation('createEvent')(UpdateEventPage)