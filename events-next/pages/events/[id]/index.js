import { useState, useEffect, useRef } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Sticky from 'react-stickynode';
import { Row, Col, Modal, Button, Skeleton, Image as AntdImage } from 'antd';
import Container from 'components/UI/Container/Container';
import Loader from 'components/Loader/Loader';
import { getDeviceType } from 'library/helpers/get_device_type';
import GetAPIData, { ProcessAPIData } from 'library/helpers/get_api_data';
import Description from 'container/SinglePage/Description/Description';
// import Amenities from 'container/SinglePage/Amenities/Amenities';
// import Location from 'container/SinglePage/Location/Location';
// import Review from 'container/SinglePage/Review/Review';
import Reservation from 'container/SinglePage/Reservation/Reservation';
import BottomReservation from 'container/SinglePage/Reservation/BottomReservation';
import TopBar from 'container/SinglePage/TopBar/TopBar';
import SinglePageWrapper, {
  PostImage
} from 'container/SinglePage/SinglePageView.style';
import PostImageGallery from 'container/SinglePage/ImageGallery/ImageGallery';
import { getUserSSR } from 'library/helpers/get_user_data'
import LazyLoad from 'react-lazyload'
import { withTranslation } from 'i18n'

const SingleEventPage = ({ t, processedData, deviceType, query, user, inFavorites }) => {
  const [href, setHref] = useState('')
  const [isModalShowing, setIsModalShowing] = useState(false)
  const [sceletonLoader, setSceletonLoader] = useState(true)

  const {
    // reviews,
    rating,
    ratingCount,
    price,
    currency,
    title,
    id,
    detailDesc,
    previewPicture,
    coverPicture,
    slug,
    gallery,
    categories,
    location,
    content,
    amenities,
    author
  } = processedData;

  useEffect(() => {
    if (!isEmpty(processedData)) {
      setSceletonLoader(false)
    }
    const path = window.location.href;
    setHref(path);
    return () => { }
  }, [processedData, setHref])

  return (
    <>
      <Head>
        <title>Events | {title}</title>
        {processedData.ticketsCloudId &&
          <script src="https://ticketscloud.com/static/scripts/widget/tcwidget.js" defer></script>}
      </Head>
      <SinglePageWrapper>
        {coverPicture &&
          <PostImage imageUrl={coverPicture}>
            {gallery.length > 0 && <>
              <Button
                type="primary"
                onClick={() => setIsModalShowing(true)}
                className="image_gallery_button"
              >
                View Photos
          </Button>
              <Modal
                visible={isModalShowing}
                onCancel={() => setIsModalShowing(false)}
                footer={null}
                width="100%"
                maskStyle={{
                  backgroundColor: 'rgba(255, 255, 255, 0.95)',
                }}
                wrapClassName="image_gallery_modal"
                closable={false}
              >
                <>
                  <PostImageGallery images={[coverPicture]} />
                  <Button
                    onClick={() => setIsModalShowing(false)}
                    className="image_gallery_close"
                  >
                    <svg width="16.004" height="16" viewBox="0 0 16.004 16">
                      <path
                        id="_ionicons_svg_ios-close_2_"
                        d="M170.4,168.55l5.716-5.716a1.339,1.339,0,1,0-1.894-1.894l-5.716,5.716-5.716-5.716a1.339,1.339,0,1,0-1.894,1.894l5.716,5.716-5.716,5.716a1.339,1.339,0,0,0,1.894,1.894l5.716-5.716,5.716,5.716a1.339,1.339,0,0,0,1.894-1.894Z"
                        transform="translate(-160.5 -160.55)"
                        fill="#909090"
                      />
                    </svg>
                  </Button>
                </>
              </Modal>
            </>}
          </PostImage>}
        <TopBar t={t} title={title} shareURL={href} author={author} media={`${process.env.AMAZON_ASSETS_URL}/${previewPicture}`} event={processedData} user={user} inFavorites={inFavorites} />

        <Container>
          <Row gutter={30} id="reviewSection" style={{ marginTop: 30 }}>
            <Col span={24} order={2} lg={{ span: 16, order: 1 }}>
              <Skeleton active loading={sceletonLoader} paragraph={{ rows: 10 }}>
                {!coverPicture &&
                  <div style={{ textAlign: 'center' }}>
                    <LazyLoad height={450} once resize={true}>
                      <img style={{ borderRadius: '8px', marginBottom: '20px' }}
                        alt={processedData.title}
                        src={`${process.env.AMAZON_ASSETS_URL}/${previewPicture}?width=350`} />
                    </LazyLoad>
                  </div>}
                <Description
                  content={detailDesc}
                  title={title}
                  categories={categories}
                  // location={location}
                  rating={rating}
                  ratingCount={ratingCount}
                />
              </Skeleton>
              {/* <Amenities amenities={amenities} /> */}
              {/* <Location location={{ 'lat': 123, 'lng': 432, 'formattedAddress': 'qwe' }} /> */}
            </Col>
            <Col span={24} order={1} lg={{ span: 8, order: 2 }}>
              {deviceType === 'desktop' ? (
                <Sticky
                  innerZ={999}
                  activeClass="isSticky"
                  top={202}
                  bottomBoundary="#reviewSection"
                >
                  <Reservation t={t} price={price} event={processedData} currency={currency} eventId={id} user={user} owner={processedData.owner} />
                </Sticky>
              ) : (
                  <BottomReservation
                    t={t}
                    title={title}
                    price={price}
                    rating={rating}
                    event={processedData}
                    user={user}
                    ratingCount={ratingCount}
                  />
                )}
            </Col>
          </Row>
          <Row gutter={30}>
            <Col xl={16}>
              {/* reviews here */}
              {/* <Review
                reviews={reviews}
                ratingCount={ratingCount}
                rating={rating}
              /> */}
            </Col>
            <Col xl={8} />
          </Row>
        </Container>
      </SinglePageWrapper>
    </>
  );
}

SingleEventPage.defaultProps = {
  i18nNamespaces: ['eventPage']
}

SingleEventPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export async function getServerSideProps(context) {
  const { req, query } = context;
  const user = getUserSSR(context)
  const eventId = query.id

  const apiUrl = [
    {
      endpoint: `events/${eventId}`,
      name: 'eventSingleData',
    },
  ];

  const pageData = await GetAPIData(apiUrl)
  const processedData = ProcessAPIData(pageData)
  const deviceType = getDeviceType(req)

  if ((Object.keys(processedData).length === 0
    && processedData.constructor === Object) || processedData.status === 'fail') {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  let inFavorites = false

  if (user) {
    const reqFav = await fetch(`${process.env.API_URL}/users/favorites/${eventId}`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.accessToken}`
      }
    })
    const resFav = await reqFav.json()

    if (resFav.status === 'success') {
      inFavorites = inFavorites.inFavorites
    }
  }

  return {
    props: { query, processedData, deviceType, user, inFavorites: inFavorites },
  };
}

export default withTranslation('eventPage')(SingleEventPage)
