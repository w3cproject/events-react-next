import { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import Toolbar from 'components/UI/Toolbar/Toolbar';
import CategorySearch from 'container/Listing/Search/CategorySearch/CategorySearch';
import { Checkbox } from 'antd';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import { getUserSSR } from 'library/helpers/get_user_data'
import GetAPIData, {
  ProcessAPIData,
} from 'library/helpers/get_api_data';
import NumberFormat from 'react-number-format'
import { getDeviceType } from 'library/helpers/get_device_type';
import Booking from 'container/SinglePage/Booking/Booking'
import { Link, withTranslation } from 'i18n'
import { ArrowLeftOutlined, ExportOutlined, UserOutlined } from '@ant-design/icons'
import { Row, Col, Typography, List, Avatar, Button } from 'antd';
import Container from 'components/UI/Container/Container'
import { ROLE_SUPER_ADMIN } from 'settings/constant.js'

const { Text, Title } = Typography

const EventsManagePage = ({ t, event, eventBookings, eventTickets, deviceType, user }) => {
  return (
    <>
      <Head>
        <title>Events | Events list</title>
      </Head>
      <Container>
        <Row gutter={30} style={{ marginTop: 30 }}>
          <Col span={24} style={{ marginBottom: '30px' }}>
            <Link href={`/events/${event.id}`}>
              <a>
                <Text>
                  <ArrowLeftOutlined style={{ marginRight: '5px' }} />
                    Назад к мероприятию
                  </Text>
              </a>
            </Link>
          </Col>
          <Col span={24} style={{ marginBottom: '30px' }}>
            <Title>{event.title}</Title>
            <div style={{ textAlign: 'right', display: 'flex', justifyContent: 'space-between', marginBottom: '20px' }}>
              <div style={{ textAlign: 'left' }}>
                <span>{event.price > 0 ? 'Всего куплено: ' : 'Всего зарегистрировано: '}<strong>{eventTickets}</strong></span><br />
                <NumberFormat value={eventTickets * event.price}
                  displayType={'text'}
                  prefix='На сумму: '
                  suffix={`${event.currency === 'rub' ? '₽' : '$'}`}
                  thousandSeparator={true} />
              </div>
              <a href={`${process.env.API_URL}/events/${event.id}/export`}>
                <Button type="primary" icon={<ExportOutlined />}>
                  Экспорт
                </Button>
              </a>
            </div>
            <List
              itemLayout="horizontal"
              dataSource={eventBookings}
              renderItem={user => (
                <List.Item>
                  <List.Item.Meta
                    UserOutlined
                    avatar={
                      user.profilePicture
                        ? <Avatar src={process.env.AMAZON_ASSETS_URL + '/' + user.profilePicture} />
                        : <Avatar style={{ backgroundColor: '#1890ff' }} icon={<UserOutlined />} />}
                    title={<a href="https://ant.design">{user.firstName ? user.firstName + ' ' + user.lastName : ''}</a>}
                    description={
                      <>
                        <span>Email: {user.email}</span><br />
                        {user.phone && <><span>Тел: {user.phone}</span><br /></>}
                        <span>Билетов: {user.tickets.filter(ticket => { return ticket.eventId === event.id }).length}</span><br />
                      </>
                    }
                  />
                </List.Item>
              )}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}

EventsManagePage.defaultProps = {
  i18nNamespaces: ['eventPage']
}

EventsManagePage.propTypes = {
  t: PropTypes.func.isRequired,
}

export async function getServerSideProps(context) {
  const { req, query } = context;
  const user = getUserSSR(context)
  const eventId = query.id

  if (!user) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  const apiUrl = [
    {
      endpoint: `events/${eventId}`,
      name: 'eventSingleData',
    },
    {
      endpoint: `events/${eventId}/bookings`,
      name: 'eventBookings',
    },
  ];

  const pageData = await GetAPIData(apiUrl, 'GET', user.accessToken);

  let event = {},
    eventBookings = [],
    eventTickets = null;

  if (pageData) {
    pageData.forEach((item, key) => {
      if (item.name === 'eventSingleData') {
        event = item.data.event;
      } else if (item.name === 'eventBookings') {
        eventBookings = item.data ? [...item.data.bookings] : [];
        eventTickets = item.data.count
      }
    });
  }

  if (event.owner.id !== user.id && !user.roles.includes(ROLE_SUPER_ADMIN)) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  const deviceType = getDeviceType(req);
  return {
    props: { event, eventBookings, eventTickets, deviceType, user },
  };
}

export default withTranslation('eventPage')(EventsManagePage)