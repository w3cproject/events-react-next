import { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import Toolbar from 'components/UI/Toolbar/Toolbar';
import CategorySearch from 'container/Listing/Search/CategorySearch/CategorySearch';
import { Checkbox } from 'antd';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import { PostPlaceholder } from 'components/UI/ContentLoader/ContentLoader';
import { getUserSSR } from 'library/helpers/get_user_data'
import GetAPIData, {
  ProcessAPIData,
} from 'library/helpers/get_api_data';
import { getDeviceType } from 'library/helpers/get_device_type';
import Booking from 'container/SinglePage/Booking/Booking'
import { withTranslation } from 'i18n'

const EventsListPage = ({ t, processedData, deviceType, authUser, userProfile }) => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();

    const res = await fetch(`${process.env.API_URL}/events/${eventId}/buy`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        ticketsCount: formState.ticketsCount
      })
    })
    const data = await res.json()
  };

  return (
    <>
      <Head>
        <title>Events | Events list</title>
      </Head>
      <Booking t={t} event={processedData} authUser={authUser} userProfile={userProfile} />
    </>
  );
}

EventsListPage.defaultProps = {
  i18nNamespaces: ['eventPage']
}

EventsListPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export async function getServerSideProps(context) {
  const { req, query } = context;
  const authUser = getUserSSR(context)
  const eventId = query.id
  
  let eventData;
  let userProfile = null
  
  let apiUrl = [
    {
      endpoint: `events/${eventId}`,
      name: 'eventSingleData',
    },
  ];
  
  if (authUser) {
    apiUrl.push({
      endpoint: `users/${authUser.id}`,
      name: 'userProfile'
    })
  }

  const pageData = await GetAPIData(apiUrl);

  if (pageData) {
    pageData.forEach((item, key) => {
      if (item.name === 'eventSingleData') {
        eventData = item.data.event ?? {};
      } 
      if (authUser && item.name === 'userProfile') {
        userProfile = item.data.user ?? {};
      }
    });
  }

  if (eventData.ticketsCloudId) {
    context.res.writeHead(302, { Location: `/events/${eventId}` });
    context.res.end();
  }

  const deviceType = getDeviceType(req);
  return {
    props: { processedData: eventData, deviceType, authUser, userProfile },
  };
}

export default withTranslation('eventPage')(EventsListPage)