import React, { useContext } from 'react';
import Head from 'next/head';
import { AgentDetailsViewPage } from 'container/UserCompanyPage';
import GetAPIData, { ProcessAPIData, FetchWithToken } from 'library/helpers/get_api_data';
import { getDeviceType } from 'library/helpers/get_device_type';
import { AuthContext } from 'context/AuthProvider';
import { parseCookies } from 'nookies'
import { getUserSSR } from 'library/helpers/get_user_data'


export default function profilePage(props) {
  const { userProfile } = props
  return (
    <>
      <Head>
        <title>
          {userProfile.firstName
            ? userProfile.firstName + ' ' + userProfile.lastName
            : 'Страница организатора'}
        </title>
      </Head>
      <AgentDetailsViewPage {...props} />
    </>
  );
}

export async function getServerSideProps(context) {
  const { req, query } = context;
  const { user_id } = query

  const apiUrl = [
    {
      endpoint: `users/${user_id}`,
      name: 'userProfile',
    },
    {
      endpoint: `users/${user_id}/events`,
      name: 'userEvents',
    }
  ];

  const pageData = await GetAPIData(apiUrl);

  let userProfile = {},
    userEvents = [];

  if (pageData) {
    pageData.forEach((item, key) => {
      if (item.name === 'userProfile') {
        userProfile = item.data.user;
      } else if (item.name === 'userEvents') {
        userEvents = item.data ? [...item.data.events] : [];
      }
    });
  }

  // const processedData = ProcessAPIData(pageData);
  const deviceType = getDeviceType(req);
  return {
    props: { userProfile, userEvents, deviceType },
  };
}
