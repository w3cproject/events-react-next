import React from 'react';
import { withTranslation, Link } from 'i18n'
import PropTypes from 'prop-types'
import Image from 'components/UI/Image/Image';
import Heading from 'components/UI/Heading/Heading';
import NotFoundWrapper, { ContentWrapper } from 'container/404/404.style';
import Image404 from 'assets/images/404@2x.png';
import { Result, Button, Typography } from 'antd';
import { CloseCircleOutlined } from '@ant-design/icons';
import { LOGIN_PAGE } from 'settings/constant.js'

const { Paragraph, Text } = Typography;

const PaymentsCanceledPage = ({ t }) => {
  return (
    <Result
      status="error"
      title={t('canceledPayment')}
      extra={[
        <Link href="/">
          <a>
            <Button type="primary" key="console">
              {t('BackToHome')}
            </Button>
          </a>
        </Link>,
        <Link href={LOGIN_PAGE}>
          <a>
            <Button key="buy">{t('login')}</Button>
          </a>
        </Link>,
      ]}
    >
      <div className="desc" style={{ textAlign: 'center' }}>
        <Paragraph>
          <Text
            strong
            style={{
              fontSize: 16,
            }}
          >
            {t('canceledMessage')}
          </Text>
        </Paragraph>
        <Paragraph>
          <CloseCircleOutlined className="site-result-demo-error-icon" style={{ color: '#ff4d4f' }} /> {t('wrongCard')}
        </Paragraph>
        <Paragraph>
          <CloseCircleOutlined className="site-result-demo-error-icon" style={{ color: '#ff4d4f' }} /> {t('wrongMoney')}
        </Paragraph>
      </div>
    </Result>
  );
};

PaymentsCanceledPage.defaultProps = {
  i18nNamespaces: ['paymentsPage']
}

PaymentsCanceledPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('paymentsPage')(PaymentsCanceledPage)
