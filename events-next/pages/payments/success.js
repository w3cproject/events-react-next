import React from 'react';
import { withTranslation, Link } from 'i18n'
import PropTypes from 'prop-types'
import Image from 'components/UI/Image/Image';
import Heading from 'components/UI/Heading/Heading';
import NotFoundWrapper, { ContentWrapper } from 'container/404/404.style';
import Image404 from 'assets/images/404@2x.png';
import { Result, Button } from 'antd';
import { LOGIN_PAGE } from 'settings/constant.js'

const PaymentsSuccessPage = ({ t }) => {
  return (
    <Result
      status="success"
      title={t('succeededPayment')}
      subTitle={t('successMessage')}
      extra={[
        <Link href="/">
          <a>
            <Button type="primary" key="console">
              {t('BackToHome')}
            </Button>
          </a>
        </Link>,
        <Link href={LOGIN_PAGE}>
          <a>
            <Button key="buy">{t('login')}</Button>
          </a>
        </Link>,
      ]}
    />
  );
};

PaymentsSuccessPage.defaultProps = {
  i18nNamespaces: ['paymentsPage']
}

PaymentsSuccessPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('paymentsPage')(PaymentsSuccessPage)
