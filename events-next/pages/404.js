// import ErrorPage from '../container/404/404';
import React from 'react';
import { withTranslation, Link } from 'i18n'
import PropTypes from 'prop-types'
import Image from 'components/UI/Image/Image';
import Heading from 'components/UI/Heading/Heading';
import NotFoundWrapper, { ContentWrapper } from 'container/404/404.style';
import Image404 from 'assets/images/404@2x.png';

const ErrorPage = ({ t }, props) => {
  const { errorCode } = props;
  let content = 'An unexpected error has occurred';
  if (errorCode === 400) {
    content = `${errorCode} : Bad Request`;
  } else if (errorCode === 404) {
    content = `${errorCode} : This page could not be found`;
  } else if (errorCode === 405) {
    content = `${errorCode} : Method Not Allowed`;
  } else if (errorCode === 500) {
    content = `${errorCode} : Internal Server Error`;
  }

  return (
    <NotFoundWrapper>
      <ContentWrapper>
        <Image src={Image404} alt={String(errorCode)} />
        <Heading as="h2" content={content} />
        <Link href="/">
          <a>{t('BackToHome')}</a>
        </Link>
      </ContentWrapper>
    </NotFoundWrapper>
  );
};

/* ErrorPage.getInitialProps = async () => ({
    namespacesRequired: ['common']
}) */
ErrorPage.defaultProps = {
  i18nNamespaces: ['404Page']
}

ErrorPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('404Page')(ErrorPage)
