import 'react-image-gallery/styles/css/image-gallery.css'
import 'react-dates/lib/css/_datepicker.css'
import 'react-multi-carousel/lib/styles.css'
import '@glidejs/glide/dist/css/glide.core.min.css'
import 'antd/lib/date-picker/style/index.css'
import { ThemeProvider } from 'styled-components'
import theme from 'themes/default.theme'
import GlobalStyles from 'assets/style/Global.style'
import Layout from 'container/Layout/Layout'
import AuthProvider from 'context/AuthProvider'
import { SearchProvider } from 'context/SearchProvider'
import 'antd/dist/antd.css'
import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import App from 'next/app'
import { appWithTranslation } from 'i18n'
import { YMInitializer } from 'react-yandex-metrika'

function MyApp({ Component, router, pageProps }) {
  const { query } = router

  return (
    <>
      {process.env.NODE_ENV === 'production' &&
        <YMInitializer accounts={[64896541]} options={{ defer: true, webvisor: true }} version="2" />}
      <AuthProvider>
        <SearchProvider query={query}>
          <ThemeProvider theme={theme}>
            <Layout>
              <GlobalStyles />
              <Component {...pageProps} />
            </Layout>
          </ThemeProvider>
        </SearchProvider>
      </AuthProvider>
    </>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext)
  // const request = appContext.ctx.req;
  const defaultProps = appContext.Component.defaultProps

  // if (request) {
  //   request.cookies = cookie.parse(request.headers.cookie || '');
  //   authenticated = !!request.cookies.session;
  // }

  return {
    ...appProps,
    pageProps: {
      namespacesRequired: [...(appProps.pageProps.namespacesRequired || []), ...(defaultProps?.i18nNamespaces || [])]
    }
  }
}


export default appWithTranslation(MyApp)
