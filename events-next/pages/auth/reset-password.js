import React from 'react';
import Head from 'next/head';
import ResetPassWord from 'container/Auth/ResetPassword';
import { getUserSSR } from 'library/helpers/get_user_data'

export default function forgetPasswordPage({ token }) {
  return (
    <>
      <Head>
        <title>Reset password .</title>
      </Head>
      <ResetPassWord token={token} />
    </>
  );
}

export async function getServerSideProps(context) {
  const { t } = context.query
  const user = getUserSSR(context)

  if (user || !t) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  return {
    props: { token: t },
  };
}
