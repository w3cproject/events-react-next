import React from 'react';
import Head from 'next/head';
import { withTranslation } from 'i18n';
import PropTypes from 'prop-types'
import SignIn from 'container/Auth/SignIn/SignIn';
import { getUserSSR } from 'library/helpers/get_user_data'

const SignInPage = ({ t }) => {
  return (
    <>
      <Head>
        <title>{t('pageTitle')} .</title>
      </Head>
      <SignIn t={t} />
    </>
  );
}

SignInPage.defaultProps = {
  i18nNamespaces: ['signInPage']
}

SignInPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('signInPage')(SignInPage)

export async function getServerSideProps(context) {
  const user = getUserSSR(context)

  if (user) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  return {
    props: {},
  };
}