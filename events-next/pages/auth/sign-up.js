import React from 'react';
import Head from 'next/head';
import { withTranslation } from 'i18n';
import PropTypes from 'prop-types'
import SignUp from 'container/Auth/SignUp/SignUp';
import { getUserSSR } from 'library/helpers/get_user_data'

const SignUpPage = ({ t }) => {
  return (
    <>
      <Head>
        <title>{t('pageTitle')} .</title>
      </Head>
      <SignUp t={t} />
    </>
  );
}

SignUpPage.defaultProps = {
  i18nNamespaces: ['signUpPage']
}

SignUpPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('signUpPage')(SignUpPage)

export async function getServerSideProps(context) {
  const user = getUserSSR(context)

  if (user) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  return {
    props: {},
  };
}