import React from 'react';
import Head from 'next/head';
import ForgetPassWord from 'container/Auth/ForgetPassword';
import { getUserSSR } from 'library/helpers/get_user_data'

export default function forgetPasswordPage() {
  return (
    <>
      <Head>
        <title>Восстановление пароля .</title>
      </Head>
      <ForgetPassWord />
    </>
  );
}

export async function getServerSideProps(context) {
  const user = getUserSSR(context)

  if (user) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  return {
    props: {},
  };
}
