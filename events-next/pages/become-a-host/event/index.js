import React from 'react';
import Head from 'next/head';
import PropTypes from 'prop-types'
import AddListing from 'container/AddListing/AddListing';
import { withTranslation } from 'i18n'
import { getUserSSR } from 'library/helpers/get_user_data'

const createEventPage = ({ t, user, categoriesData }) => {
  return (
    <>
      <Head>
        <title>Events | Create Event</title>
      </Head>
      <AddListing t={t} user={user} categories={categoriesData.categories} />
    </>
  );
}

createEventPage.defaultProps = {
  i18nNamespaces: ['createEvent']
}

createEventPage.propTypes = {
  t: PropTypes.func.isRequired,
}

export async function getServerSideProps(context) {
  const user = getUserSSR(context)

  if (!user) {
    context.res.writeHead(302, { Location: '/auth/sign-in' })
    context.res.end()
  }

  const res = await fetch(`${process.env.API_URL}/categories`)
  const data = await res.json()

  return {
    props: { user, categoriesData: data },
  };
}

export default withTranslation('createEvent')(createEventPage)