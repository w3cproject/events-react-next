import React, { useContext } from 'react';
import Head from 'next/head';
import { AgentDetailsViewPage } from 'container/Agent/index';
import GetAPIData, { ProcessAPIData, FetchWithToken } from 'library/helpers/get_api_data';
import PropTypes from 'prop-types';
import { withTranslation } from 'i18n'
import { getDeviceType } from 'library/helpers/get_device_type';
import { AuthContext } from 'context/AuthProvider';
import { parseCookies } from 'nookies'
import { getUserSSR } from 'library/helpers/get_user_data'


const ProfilePage = (props) => {
  return (
    <>
      <Head>
        <title>Profile</title>
      </Head>
      <AgentDetailsViewPage {...props} />
    </>
  );
}

ProfilePage.defaultProps = {
  i18nNamespaces: ['profilePage']
}

ProfilePage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('profilePage')(ProfilePage)

export async function getServerSideProps(context) {
  const { req } = context;
  const user = getUserSSR(context)

  if (!user) {
    context.res.writeHead(302, { Location: '/' })
    context.res.end()
  }

  const apiUrl = [
    {
      endpoint: `users/${user.id}`,
      name: 'userProfile',
    },
    {
      endpoint: `users/${user.id}/bookings`,
      name: 'userBookings',
    },
    {
      endpoint: `users/favorites`,
      name: 'userFavorites',
    },
    {
      endpoint: `users/${user.id}/payments`,
      name: 'userPayments',
    },
  ];

  const pageData = await GetAPIData(apiUrl, 'GET', user.accessToken);

  let userProfile = {},
    userBookings = [],
    userFavorites = [],
    userPayments = [];

  if (pageData) {
    pageData.forEach((item, key) => {
      if (item.name === 'userProfile') {
        userProfile = item.data.user;
      } else if (item.name === 'userBookings') {
        userBookings = item.data ? [...item.data.bookings] : [];
      } else if (item.name === 'userFavorites') {
        userFavorites = item.data ? [...item.data.favorites] : [];
      } else if (item.name === 'userPayments') {
        userPayments = item.data ? [...item.data.payments] : [];
      }
    });
  }

  // const processedData = ProcessAPIData(pageData);
  const deviceType = getDeviceType(req);
  return {
    props: { userProfile, userBookings, userFavorites, userPayments, deviceType },
  };
}
