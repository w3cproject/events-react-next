import React from 'react';
import Head from 'next/head';
import { AgentAccountSettingsPage } from 'container/Agent/';
import GetAPIData, { ProcessAPIData } from 'library/helpers/get_api_data';
import { getUserSSR } from 'library/helpers/get_user_data'
import { parseCookies } from 'nookies'


export default function accountSettingsPage({ processedData, user }) {
  return (
    <>
      <Head>
        <title>Account Settings .</title>
      </Head>
      <AgentAccountSettingsPage processedData={processedData} user={user} />
    </>
  );
}

export async function getServerSideProps(ctx) {
  const user = getUserSSR(ctx)

  if (!user) {
    ctx.res.writeHead(302, { Location: '/' })
    ctx.res.end()
  }

  const apiUrl = [
    {
      endpoint: 'auth/user',
      name: 'userProfile',
    },
  ];

  const pageData = await GetAPIData(apiUrl, 'GET', user.accessToken);
  const processedData = ProcessAPIData(pageData);
  return {
    props: { processedData, user },
  };
}
