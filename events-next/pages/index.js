import Head from 'next/head';
import { Link } from 'i18n';
import PropTypes from 'prop-types'
import Container from 'components/UI/Container/Container';
import Heading from 'components/UI/Heading/Heading';
import SectionTitle from 'components/SectionTitle/SectionTitle';
import SearchArea from 'container/Home/Search/Search';
import LocationGrid from 'container/Home/Location/Location';
import SectionGrid from 'components/SectionGrid/SectionGrid';
import GetAPIData from 'library/helpers/get_api_data';
import { getDeviceType } from 'library/helpers/get_device_type';
import { LISTING_POSTS_PAGE, SINGLE_POST_PAGE, EVENTS_LIST_PAGE } from 'settings/constant';
import { CATEGORY_CONCERTS, CATEGORY_TREATRES, CATEGORY_STANDUP } from 'settings/categories-constants'
import {
  HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_MOBILE_DEVICE,
  HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_TABLET_DEVICE,
  HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_DESKTOP_DEVICE,
  HOME_PAGE_SECTIONS_COLUMNS_RESPONSIVE_WIDTH,
} from 'settings/config';
import { i18n, withTranslation } from 'i18n'

const HomePage = ({
  deviceType,
  locationData,
  listingConcerts,
  listingTheatres,
  listingConcertsThisWeek,
  categories,
  t
}) => {
  let limit;

  if (deviceType === 'mobile') {
    limit = HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_MOBILE_DEVICE;
  }
  if (deviceType === 'tablet') {
    limit = HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_TABLET_DEVICE;
  }

  if (deviceType === 'desktop') {
    limit = HOME_PAGE_SECTIONS_ITEM_LIMIT_FOR_DESKTOP_DEVICE;
  }
  return (
    <>
      <Head>
        <title>Мероприятия .</title>
      </Head>
      <SearchArea categories={categories} />
      {/* <LocationGrid data={locationData} deviceType={deviceType} /> */}
      <Container fluid={true}>
        <SectionTitle
          title={<Heading content={`События в ближайшие дни`} />}
          link={
            <Link href={`${EVENTS_LIST_PAGE}?categories=${CATEGORY_CONCERTS}`}>
              <a>{t('showAll')}</a>
            </Link>
          }
        />
        <SectionGrid
          link={EVENTS_LIST_PAGE}
          columnWidth={HOME_PAGE_SECTIONS_COLUMNS_RESPONSIVE_WIDTH}
          data={listingConcerts}
          limit={limit}
          deviceType={deviceType}
        />
        <SectionTitle
          title={<Heading content={`Самые ожидаемые концерты`} />}
          link={
            <Link href={`${EVENTS_LIST_PAGE}?categories=${CATEGORY_CONCERTS}`}>
              <a>{t('showAll')}</a>
            </Link>
          }
        />
        <SectionGrid
          link={EVENTS_LIST_PAGE}
          columnWidth={HOME_PAGE_SECTIONS_COLUMNS_RESPONSIVE_WIDTH}
          data={listingConcertsThisWeek}
          limit={limit}
          deviceType={deviceType}
        />
        <SectionTitle
          title={<Heading content="Театр: выбор зрителей" />}
          link={
            <Link href={`${EVENTS_LIST_PAGE}?categories=${CATEGORY_STANDUP},${CATEGORY_TREATRES}`}>
              <a>{t('showAll')}</a>
            </Link>
          }
        />
        <SectionGrid
          link={EVENTS_LIST_PAGE}
          columnWidth={HOME_PAGE_SECTIONS_COLUMNS_RESPONSIVE_WIDTH}
          data={listingTheatres.slice(0, limit)}
          limit={limit}
          deviceType={deviceType}
        />
      </Container>
    </>
  );
}

export async function getServerSideProps(context) {
  const { req } = context;
  const apiUrl = [
    {
      endpoint: `events?categories=${CATEGORY_STANDUP},${CATEGORY_CONCERTS}&limit=6`,
      name: 'listingConcerts',
    },
    {
      endpoint: `events?categories=${CATEGORY_TREATRES}&limit=6`,
      name: 'listingTheatres',
    },
    {
      endpoint: `events?categories=${CATEGORY_STANDUP},${CATEGORY_CONCERTS}&limit=6&offset=6`,
      name: 'listingConcertsThisWeek',
    },
    {
      endpoint: `categories`,
      name: 'categoriesList',
    },
    /* {
      endpoint: 'location',
      name: 'locationData',
    }, */
  ];
  const deviceType = getDeviceType(req);
  const pageData = await GetAPIData(apiUrl);

  let locationData = [],
    listingConcerts = [],
    listingTheatres = [],
    listingConcertsThisWeek = [],
    categories = [];

  if (pageData) {
    pageData.forEach((item, key) => {
      if (item.name === 'locationData') {
        locationData = item.data ? [...item.data] : [];
      } else if (item.name === 'listingConcerts') {
        listingConcerts = item.data ? [...item.data.events] : [];
      } else if (item.name === 'listingTheatres') {
        listingTheatres = item.data ? [...item.data.events] : [];
      } else if (item.name === 'listingConcertsThisWeek') {
        listingConcertsThisWeek = item.data ? [...item.data.events] : [];
      } else if (item.name === 'categoriesList') {
        categories = item.data ? [...item.data.categories] : [];
      }
    });
  }
  return {
    props: { deviceType, locationData, listingConcerts, listingTheatres, listingConcertsThisWeek, categories }
  };
}

/* HomePage.getInitialProps = async () => ({
  namespacesRequired: ['common'],
}) */

HomePage.defaultProps = {
  i18nNamespaces: ['indexPage']
}

HomePage.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation('indexPage')(HomePage)