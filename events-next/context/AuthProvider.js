import React, { useState, useEffect } from 'react';
import { Router } from 'i18n';
import { setCookie, parseCookies, destroyCookie } from 'nookies'
import Cookies from 'js-cookie'

export const AuthContext = React.createContext();

const AuthProvider = (props) => {
  const [loggedIn, setLoggedIn] = useState(false);
  const [user, setUser] = useState({});
  const cookies = parseCookies()

  useEffect(() => {
    if (cookies.user) {
      setUser(JSON.parse(cookies.user))
      setLoggedIn(true)
    }
    return () => { }
  }, [])

  const signIn = (params) => {
    let userObj = {
      name: params.user.email,
      accessToken: params.user.access_token,
      id: params.user.id,
      avatar: params.user.profilePicture,
      roles: params.user.roles
    }
    setUser(userObj);
    setLoggedIn(true);
    setCookie(null, 'user', JSON.stringify(userObj), {
      maxAge: 30 * 24 * 60 * 60, // 30 days
      path: '/'
    })
    Router.push(`/profile`);
  };

  const signUp = (params) => {
    let userObj = {
      name: params.email,
      accessToken: params.access_token,
      id: params.id,
      avatar: params.profilePicture,
      roles: params.roles
    }
    setUser(userObj);
    setLoggedIn(true);
    setCookie(null, 'user', JSON.stringify(userObj), {
      maxAge: 30 * 24 * 60 * 60, // 30 days
      path: '/'
    })
    Router.push(`/profile`);
  };

  const logOut = () => {
    setUser(null);
    setLoggedIn(false);
    destroyCookie(null, 'user');
    destroyCookie(null, 'accessToken');
    Cookies.remove('user')
    Cookies.remove('accessToken')
    Router.push('/')
  };

  return (
    <AuthContext.Provider
      value={{
        loggedIn,
        logOut,
        signIn,
        signUp,
        user
      }}
    >
      <>{props.children}</>
    </AuthContext.Provider>
  );
};

export default AuthProvider;
