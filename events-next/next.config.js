const { nextI18NextRewrites } = require('next-i18next/rewrites')

const localeSubpaths = {
  'en': 'en',
  'he': 'he',
  'zh-cn': 'zh'
}

const withPlugins = require('next-compose-plugins');
const withTM = require('next-transpile-modules')(["@events/next"]);
const withOptimizedImages = require('next-optimized-images');
const withFonts = require('next-fonts');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');

// require('dotenv').config({
//   path: path.resolve(
//     __dirname,
//     `.env.${process.env.NODE_ENV}`,
//   ),
// });

const nextConfig = withTM({
  target: 'serverless',
  env: {
    GOOGLE_API_KEY: 'YOUR_GOOGLE_MAP_API_KEY',
    REACT_APP_GOOGLE_MAP_API_KEY:
      'https://maps.googleapis.com/maps/api/js?v=3.exp&key=YOUR_GOOGLE_MAP_API_KEY&libraries=geometry,drawing,places',
    API_URL: process.env.API_URL,
    TICKETS_CLOUD_TOKEN: process.env.TICKETS_CLOUD_TOKEN,
    AMAZON_ASSETS_URL: process.env.AMAZON_ASSETS_URL
  },
  webpack: (config, { isServer }) => {
    // HOTFIX: https://github.com/webpack-contrib/mini-css-extract-plugin/issues/250
    config.plugins.push(
      new FilterWarningsPlugin({
        exclude: /mini-css-extract-plugin[^]*Conflicting order between:/,
      })
    );
    config.resolve.modules.push(__dirname);

    return config;
  },
  rewrites: async () => nextI18NextRewrites(localeSubpaths),
  publicRuntimeConfig: {
    localeSubpaths
  },
});


module.exports = withPlugins(
  [
    [
      withOptimizedImages,
      {
        mozjpeg: {
          quality: 90,
        },
        webp: {
          preset: 'default',
          quality: 90,
        },
      },
    ],
    withFonts,
  ],
  nextConfig
);
