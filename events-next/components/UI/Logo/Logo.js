import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'i18n';
import LogoArea from './Logo.style';
import { Avatar } from 'antd'
import { UserOutlined } from '@ant-design/icons'

const LogoNext = ({ className, withLink, linkTo, colorTypeClass, title, src }) => {
  return (
    <LogoArea className={className}>
      {withLink ? (
        <Link href={linkTo}>
          <a>
            {src ?
              <img src={src} alt="Events" className={`${colorTypeClass}`} />
              : <Avatar size={40} icon={<UserOutlined />} />}
            {/* {title && <h3>{title}</h3>} */}
          </a>
        </Link>
      ) : (
          <Fragment>
            {src ?
              <img src={src} alt="Events" />
              : <Avatar size={40} icon={<UserOutlined />} style={{ color: '#f56a00', backgroundColor: '#fde3cf' }} />}
            {/* {title && <h3>{title}</h3>} */}
          </Fragment>
        )}
    </LogoArea>
  );
};

LogoNext.propTypes = {
  className: PropTypes.string,
  withLink: PropTypes.bool,
  src: PropTypes.string,
  title: PropTypes.string,
  linkTo: PropTypes.string,
  colorTypeClass: PropTypes.string,
};

export default LogoNext;
