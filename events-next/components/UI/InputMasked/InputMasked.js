import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';

export const inputPhoneMask = [/[1-9]/, ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, '-', /\d/, /\d/]

const InputMask = ({ mask, placeholder, placeholderChar, showMask, guide, keepCharPositions, name, value, onChange }) => {
  return (
    <MaskedInput
      className="ant-input"
      name={name}
      mask={mask}
      value={value}
      placeholderChar={placeholderChar}
      keepCharPositions={keepCharPositions}
      placeholder={placeholder}
      showMask={showMask}
      guide={guide}
      onChange={ e => {
        e.persist()
        onChange(e.target.value)
      }}
    />
  );
};

InputMask.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  mask: PropTypes.array,
  className: PropTypes.string,
  placeholderChar: PropTypes.string,
  placeholder: PropTypes.string,
  showMask: PropTypes.bool,
  guide: PropTypes.bool,
  keepCharPositions: PropTypes.bool,
};

InputMask.defaultProps = {
  showMask: false,
  guide: false,
  keepCharPositions: true
};

export default InputMask;
