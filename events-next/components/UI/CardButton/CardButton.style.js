import styled from 'styled-components';

export const WrapperButton = styled.button`
  box-shadow: 0 2px 20px rgba(0, 0, 0, 0.16);
  background-color: transparent;
  padding: 20px 40px;
  border-radius: 8px;
  border: 0;
  max-width: 320px;
  text-align: left;
  cursor: pointer;

  &.selected {
    border: 2px solid;
    padding: 18px 38px;
  }

  &:focus {
    outline: none;
  }
`;
export const Header = styled.header`
  
`;

export const Content = styled.div``;
export const Footer = styled.footer`
  
`;
