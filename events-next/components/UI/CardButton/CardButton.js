import React from 'react';
import PropTypes from 'prop-types'
import { WrapperButton, Header, Content, Footer } from './CardButton.style'
import { FaGlobe } from 'react-icons/fa'

const CardButton = ({
  title,
  className,
  header,
  content,
  children,
  footer,
  isSubmit,
  faIcon
}) => {
  return (
    <WrapperButton className={className} type={isSubmit ? 'submit' : 'button'}>
      {faIcon}
      <Header>{header ? header : <h2>{title}</h2>}</Header>
      <Content>{content ? content : children}</Content>
      {footer && <Footer>{footer}</Footer>}
    </WrapperButton>
  );
}

CardButton.propTypes = {
  isSubmit: PropTypes.bool,
  faIcon: PropTypes.object
};

export default CardButton