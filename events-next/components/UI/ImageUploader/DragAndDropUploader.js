import React, { useState } from 'react';
import { IoIosCloudUpload } from 'react-icons/io';
import { LoadingOutlined } from '@ant-design/icons';
import { Upload, message } from 'antd';
import styled from 'styled-components';

const DraggerWrapper = styled.div`
  height: 100%;
  .ant-upload.ant-upload-drag .ant-upload {
    padding: 0;
  }

  .ant-upload-drag-container {
    height: 500px;
  }
  .ant-upload-drag-container img {
    height: 100%;
    object-fit: cover;
    width: 100%;
  }
  .ant-upload-drag-container img:hover {
    opacity: .6;
    // margin-top: -15px;
  }
`;

const { Dragger } = Upload;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

const DragAndDropUploader = ({ onUploadChange, imageExist }) => {
  const [base64Image, setBase64Image] = useState(imageExist)
  const [loading, setLoading] = useState(false)
  const uploadTpl = (
    <>
      <div className="ant-upload-drag-icon">
        {loading ? <LoadingOutlined /> : <IoIosCloudUpload />}
      </div>
      <p className="ant-upload-text">
        Drag & drop to your image assets or browse
              </p>
    </>
  )

  const props = {
    name: 'file',
    multiple: false,
    action: `${process.env.API_URL}/loader/events`,
    listType: 'picture',
    showUploadList: false,
    // defaultFileList: photos,
    beforeUpload(file) {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
      }
      return isJpgOrPng && isLt2M;
    },
    onChange(info) {
      if (info.file.status === 'uploading') {
        setBase64Image(null)
        setLoading(true)
        // this.setState({ loading: true });
        return;
      }
      if (info.file.status === 'done') {
        // Get this url from response in real world.
        getBase64(info.file.originFileObj, imageUrl =>
          setBase64Image(imageUrl)
        );
        onUploadChange(info.file.response)
        setLoading(false)
      }
    },
  };

  return (
    <DraggerWrapper className="drag_and_drop_uploader">
      <Dragger accept=".jpg,.jpeg,.png" {...props} className="uploader">
        {base64Image
          ? <img src={base64Image} />
          : uploadTpl}
      </Dragger>
    </DraggerWrapper>
  );
};

export default DragAndDropUploader;
