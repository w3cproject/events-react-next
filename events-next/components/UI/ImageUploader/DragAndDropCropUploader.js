import React, { useState } from 'react';
import { IoIosCloudUpload } from 'react-icons/io';
import { LoadingOutlined } from '@ant-design/icons';
import { Upload, message } from 'antd';
import styled from 'styled-components';
import ImgCrop from 'antd-img-crop'

const DraggerWrapper = styled.div`
  height: 100%;

  .ant-upload.ant-upload-drag .ant-upload {
    padding: 0;
  }

  .ant-upload-drag-container img:hover {
    opacity: .6;
    // margin-top: -15px;
  }
`;

const { Dragger } = Upload;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const handleChange = info => {
  if (info.file.status === 'uploading') {
    setLoading(true)
    // this.setState({ loading: true });
    return;
  }
  if (info.file.status === 'done') {
    // Get this url from response in real world.
    getBase64(info.file.originFileObj, imageUrl =>
      base64Image = imageUrl
    );
  }
};

const DragAndDropCropUploader = ({ onUploadChange, imageExist }) => {
  const [base64Image, setBase64Image] = useState(imageExist)
  const [loading, setLoading] = useState(false)
  const uploadTpl = (
    <>
      <div className="ant-upload-drag-icon">
        {loading ? <LoadingOutlined /> : <IoIosCloudUpload />}
      </div>
      <p className="ant-upload-text">
        Drag & drop to your image assets or browse
              </p>
    </>
  )
  const props = {
    name: 'file',
    multiple: false,
    action: `${process.env.API_URL}/loader/events`,
    // action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    // defaultFileList: photos,
    listType: 'picture',
    showUploadList: false,
    beforeUpload(file) {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
      }
      const isLt2M = file.size / 1024 / 1024 < 2;
      if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
      }
      return isJpgOrPng && isLt2M;
    },
    onChange(info) {
      if (info.file.status === 'uploading') {
        setBase64Image(null)
        setLoading(true)
        // this.setState({ loading: true });
        return;
      }
      if (info.file.status === 'done') {
        // Get this url from response in real world.
        getBase64(info.file.originFileObj, imageUrl =>
          setBase64Image(imageUrl)
        );
        onUploadChange(info.file.response)
        setLoading(false)
      }
    }
    /* onChange(info) {
      console.log('state', state.data.eventDescription)
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
        getBase64(info.file.originFileObj, imageUrl =>
          state.data.previewPicture = imageUrl
        );
        onUploadChange(info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} photo uploaded successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} photo upload failed.`);
      }
    }, */
  };

  return (
    <DraggerWrapper className="drag_and_drop_uploader">
      <ImgCrop rotate aspect={270 / 450}>
        <Dragger accept=".jpg,.jpeg,.png" {...props} className="uploader">
          {base64Image
            ? <img src={base64Image} />
            : uploadTpl}
        </Dragger>
      </ImgCrop>
    </DraggerWrapper>
  );
};

export default DragAndDropCropUploader;
