import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format'
import moment from 'moment'
import { i18n } from 'i18n'
import GridCardWrapper, {
  ImageWrapper,
  FavoriteIcon,
  ContentWrapper,
  LocationArea,
  TitleArea,
  PriceArea,
  RatingArea,
  MetaWrapper,
  ButtonGroup,
} from './GridCard.style';

const GridCard = ({
  className,
  favorite,
  location,
  title,
  price,
  startDate,
  currency,
  rating,
  editBtn,
  viewDetailsBtn,
  children,
}) => {
  moment.locale(i18n.language)
  let classes = viewDetailsBtn || editBtn ? `has_btn ${className}` : className

  return (
    <GridCardWrapper className={`grid_card ${classes}`.trim()}>
      <ImageWrapper className="media_wrapper">{children}</ImageWrapper>
      <ContentWrapper className="content_wrapper">
        {location && <LocationArea>{location}</LocationArea>}
        {title && <TitleArea>{title}</TitleArea>}
        <MetaWrapper className="meta_wrapper">
          {price ?
            <PriceArea className="price">
              <NumberFormat value={price}
                displayType={'text'}
                prefix={'From '}
                suffix={`${currency === 'rub' ? '₽' : '$'} / person`}
                thousandSeparator={true} />
            </PriceArea> :
            'Free'}
          <span>{moment(startDate).fromNow()}</span>
          {rating && <RatingArea className="rating">{rating}</RatingArea>}
          {viewDetailsBtn || editBtn ? (
            <ButtonGroup className="button_group">
              {viewDetailsBtn}
              {editBtn}
            </ButtonGroup>
          ) : null}
        </MetaWrapper>
      </ContentWrapper>

      {favorite && <FavoriteIcon>{favorite}</FavoriteIcon>}
    </GridCardWrapper>
  );
};

GridCard.propTypes = {
  className: PropTypes.string,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  price: PropTypes.number,
  currency: PropTypes.string,
  rating: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  location: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  editBtn: PropTypes.element,
  viewDetailsBtn: PropTypes.element,
};

export default GridCard;
