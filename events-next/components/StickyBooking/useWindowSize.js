import { useState, useEffect } from 'react';

function getSize() {
  return {
    innerHeight: process.browser && window.innerHeight,
    innerWidth: process.browser && window.innerWidth,
    outerHeight: process.browser && window.outerHeight,
    outerWidth: process.browser && window.outerWidth,
  };
}

const useWindowSize = () => {
  let [windowSize, setWindowSize] = useState(getSize());

  function handleResize() {
    setWindowSize(getSize());
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return windowSize;
};

export default useWindowSize;
