import React from 'react';
import { Link } from 'i18n';
import { FiExternalLink } from 'react-icons/fi';
import Carousel from 'react-multi-carousel';
import Rating from 'components/UI/Rating/Rating';
import Favourite from 'components/UI/Favorite/Favorite';

import GridCard from '../GridCard/GridCard';

import productCardStyles from './ProductCard.module.css'

const responsive = {
  desktop: {
    breakpoint: {
      max: 3000,
      min: 1024,
    },
    items: 1,
    paritialVisibilityGutter: 40,
  },
  mobile: {
    breakpoint: {
      max: 464,
      min: 0,
    },
    items: 1,
    paritialVisibilityGutter: 30,
  },
  tablet: {
    breakpoint: {
      max: 1024,
      min: 464,
    },
    items: 1,
    paritialVisibilityGutter: 30,
  },
};
export default function ProductCard({
  title,
  rating,
  location,
  price,
  ratingCount,
  gallery,
  startDate,
  slug,
  id,
  link,
  deviceType,
  currency,
  unsplashPicture,
  previewPicture
}) {
  const toggleFavorites = async () => {
    const res = await fetch(`${process.env.API_URL}/users/favorites`, {
      method: 'POST',
      body: JSON.stringify({
        'eventId': id
      })
    })
    const data = await res.json()

    console.log(data)
  }

  return (
    <GridCard
      favorite={
        <Favourite
          onClick={toggleFavorites}
        />
      }
      // location={location.formattedAddress}
      title={title}
      price={price}
      startDate={startDate}
      currency={currency}
      rating={<Rating rating={rating} ratingCount={ratingCount} type="bulk" />}
    /* viewDetailsBtn={
      <Link href={`${link}/[slug]`} as={`${link}/${slug}`} prefetch={false}>
        <a>
          <FiExternalLink /> View Details
        </a>
      </Link>
    } */
    >
      <Link href={`${link}/[id]`} as={`${link}/${id}`} prefetch={false}>
        <a className={productCardStyles.absoluteLink}>
        </a>
      </Link>
      {unsplashPicture ? (
        <img
          src={`https://assets.aws.com/${unsplashPicture.amazonPath}?width=270&height=340`}
          alt={title}
          style={{
            width: '100%',
            height: '100%',
            objectFit: 'cover',
            position: 'relative',
            minHeight: '340px',
            borderRadius: '8px'
          }}
        />
      ) : (

          <img
            src={`${process.env.AMAZON_ASSETS_URL}/${previewPicture}?width=270&height=340`}
            alt={title}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'cover',
              position: 'relative',
              minHeight: '340px',
              borderRadius: '8px'
            }}
          />

        )}
      {/* <Carousel
        ssr
        additionalTransfrom={0}
        arrows
        autoPlaySpeed={3000}
        containerClass="container"
        dotListClass=""
        draggable
        focusOnSelect={false}
        infinite
        itemClass=""
        renderDotsOutside={false}
        responsive={responsive}
        deviceType={deviceType}
        showDots={true}
        sliderClass=""
        slidesToSlide={1}
      >
        {gallery.map(({ url, title }, index) => (
          <img
            src={url}
            alt={title}
            key={index}
            draggable={false}
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'cover',
              position: 'relative',
            }}
          />
        ))}
      </Carousel> */}
    </GridCard>
  );
}
