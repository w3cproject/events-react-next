import React from 'react';
import {
  FacebookShareButton,
  TwitterShareButton,
  PinterestShareButton,
  LinkedinShareButton,
  VKShareButton,
  TelegramShareButton,
  EmailShareButton
} from 'react-share';
import {
  FaTwitter,
  FaFacebookF,
  FaPinterest,
  FaLinkedinIn,
  FaVk,
  FaTelegram,
  FaAt,
  FaExternalLinkAlt
} from 'react-icons/fa';
import { Popover, Typography } from 'antd';

const { Paragraph } = Typography

export const ShortLinkShare = (props) => {
  const { event } = props;
  return (
    <Popover content="ShortLink">
      <div className="short-link">
          <Paragraph copyable={{ text: event.shortLink }}>Short Link</Paragraph>
      </div>
    </Popover>
  );
};

export const EmailShare = (props) => {
  const { title, shareURL } = props;
  return (
    <Popover content="Email">
      <div className="email">
        <EmailShareButton url={shareURL} title={title}>
          <FaAt /> Email
        </EmailShareButton>
      </div>
    </Popover>
  );
};

export const TelegramShare = (props) => {
  const { title, shareURL } = props;
  return (
    <Popover content="Telegram">
      <div className="telegram">
        <TelegramShareButton url={shareURL} title={title}>
          <FaTelegram /> Telegram
        </TelegramShareButton>
      </div>
    </Popover>
  );
};

export const VkShare = (props) => {
  const { title, shareURL, media } = props;
  return (
    <Popover content="Vk">
      <div className="vk">
        <VKShareButton url={shareURL} title={title} image={media}>
          <FaVk /> VK
        </VKShareButton>
      </div>
    </Popover>
  );
};

export const FaceBookShare = (props) => {
  const { title, shareURL } = props;
  return (
    <Popover content="Facebook">
      <div className="facebook">
        <FacebookShareButton url={shareURL} quote={title}>
          <FaFacebookF /> Facebook
        </FacebookShareButton>
      </div>
    </Popover>
  );
};

export const TwitterShare = (props) => {
  const { shareURL, title, author } = props;
  return (
    <Popover content="Twitter">
      <div className="twitter">
        <TwitterShareButton url={shareURL} title={title} via={author}>
          <FaTwitter /> Twitter
        </TwitterShareButton>
      </div>
    </Popover>
  );
};

export const LinkedInShare = (props) => {
  const { shareURL, title } = props;
  return (
    <Popover content="Twitter">
      <div className="twitter">
        <LinkedinShareButton
          url={shareURL}
          title={title}
          windowWidth={750}
          windowHeight={600}
        >
          <FaLinkedinIn /> LinkedIn
        </LinkedinShareButton>
      </div>
    </Popover>
  );
};

export const PinterestShare = (props) => {
  console.log(props)
  const { shareURL, title, media } = props;
  const mediaForPinterest = media.length ? media[0].url : [];
  return (
    <Popover content="Pinterest">
      <div className="pinterest">
        <PinterestShareButton
          url={shareURL}
          media={mediaForPinterest}
          description={title}
        >
          <FaPinterest /> Pinterest
        </PinterestShareButton>
      </div>
    </Popover>
  );
};
