import React, { useState } from 'react';
import PropTypes from 'prop-types';
import FooterWrapper, {
  MenuWrapper,
  CopyrightArea,
  SecondaryFooter,
  FooterLanguagesModalButton,
  FooterLanguagesModalList
} from './Footer.style';
import { i18n, useTranslation } from 'i18n'
import { Modal, Button } from 'antd'
import { FaGlobe } from 'react-icons/fa'

const Footer = ({ logo, menu, bgSrc, copyright, className, path }) => {
  const { t } = useTranslation('langCodes')
  const [modalState, setModalState] = useState(false)

  const hideModal = () => {
    setModalState(false)
  }

  // const handleState = (val) => {
  //   setModalState(false)
  // }

  // i18n.changeLanguage(i18n.language === 'en' ? 'ru' : 'en')
  return (
    <>
      <FooterWrapper id="footer" className={className} bg-img={bgSrc}>
        {logo && logo}
        {menu && <MenuWrapper>{menu}</MenuWrapper>}
        <FooterLanguagesModalButton>
          <Button
            type='button'
            onClick={() => setModalState(true)}
          >
            <FaGlobe /> {t(i18n.language)}
          </Button>
        </FooterLanguagesModalButton>
        {copyright && <CopyrightArea>{copyright}</CopyrightArea>}
      </FooterWrapper>
      {!!path && <SecondaryFooter />}
      <Modal
        title={t('languageChange')}
        visible={modalState}
        onOk={hideModal}
        onCancel={hideModal}
        footer={false}
      >
        <h2>{t('currentLanguage')}</h2>
        <p>{t(i18n.language)}</p>
        <h2>{t('chooseLanguage')}</h2>
        <FooterLanguagesModalList>
          {i18n.options.allLanguages.filter(lang => lang !== i18n.language)
            .map((lang, index) => (
              <Button
                onClick={() => i18n.changeLanguage(lang)} key={index}>{t(lang)}</Button>
            ))}
        </FooterLanguagesModalList>
      </Modal>
    </>
  );
};

Footer.propTypes = {
  className: PropTypes.string,
  logo: PropTypes.element,
  menu: PropTypes.element,
  bgSrc: PropTypes.string,
  copyright: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
};

export default Footer
