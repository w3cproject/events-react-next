// General Page Section
export const HOME_PAGE = '/';
export const AGENTS_PAGE = '/agents';

// Listing Single Page Section
export const EVENTS_LIST_PAGE = '/events';
// export const EVENTS_SINGLE_PAGE = '/listing';
export const LISTING_POSTS_PAGE = '/listing';
export const SINGLE_POST_PAGE = '/post';

// Agent Profile Page Section
export const USER_PROFILE_PAGE = '/profile';
export const USER_ACCOUNT_SETTINGS_PAGE = '/profile/account-settings';
export const AGENT_PROFILE_EDIT_PAGE = '/edit';
export const AGENT_IMAGE_EDIT_PAGE = '/change-image';
export const AGENT_PASSWORD_CHANGE_PAGE = '/change-password';
export const AGENT_PROFILE_DELETE = '/delete';
export const AGENT_PROFILE_FAVOURITE = '/favourite-post';
export const AGENT_PROFILE_CONTACT = '/contact';
export const ADD_EVENT_PAGE = '/become-a-host/event'
export const COMPANY_OWNER_PAGE = '/u'

// Other Pages
export const PRICING_PLAN_PAGE = '/pricing-plan';

// Login / Registation Page
export const LOGIN_PAGE = '/auth/sign-in';
export const REGISTRATION_PAGE = '/auth/sign-up';
export const CHANGE_PASSWORD_PAGE = '/auth/reset-password';
export const FORGET_PASSWORD_PAGE = '/auth/forget-password';


// USER ROLES

export const ROLE_USER = 'role_user'
export const ROLE_USER_OWNER = 'role_online_user_owner'
export const ROLE_ADMIN = 'role_admin'
export const ROLE_SUPER_ADMIN = 'role_super_admin'