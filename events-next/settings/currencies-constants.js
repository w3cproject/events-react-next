export const CURRENCIES_LIST = [
    {
        'title': 'USD',
        'value': '$'
    },
    {
        'title': 'RUB',
        'value': '₽'
    }
]