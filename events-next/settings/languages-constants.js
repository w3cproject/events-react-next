export const LANGUAGES_LIST = [
    {
        'title': 'Russian',
        'value': 'ru'
    },
    {
        'title': 'English',
        'value': 'en'
    },
    {
        'title': 'עִברִית',
        'value': 'he'
    },
    {
        'title': '中文',
        'value': 'zh'
    }
]